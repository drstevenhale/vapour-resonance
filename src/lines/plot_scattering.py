#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  PLOT_SCATTERING.PY
#
#    Steven Hale
#    2017 August 27
#    Birmingham, UK
#

"""Plots scattering and absorption profiles of potassium at a range of
vapour temperatures.

.. image:: images/transmit.png
   :width: 600
   :alt: Plot of transmission absorption profiles at a range of temperatures.

.. image:: images/scatter.png
   :width: 600
   :alt: Plot of scattering profiles at a range of temperatures.

"""

import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors

import core.plot as plot
import core.constants as constants
from core.topticadlcdlpro import TopticaDLCDLpro

########################################################################
########################################################################
########################################################################

def main(detector, array_temps):

    """
    Plots scattering and absorption profiles of potassium at a range of
    vapour temperatures.
    """

    # pylint: disable=too-many-locals

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('normal')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1)

    num_colors = range(len(array_temps))

    my_colors = [(0, 0, 1), (0.3, 0.7, 0.3), (1, 0.8, 0), (1, 0, 0)]  # B -> Y -> R
    cmap_name = 'my_colors'
    cmap = colors.LinearSegmentedColormap.from_list(cmap_name, my_colors)
    plt.register_cmap(cmap=cmap)

    cmap = plt.get_cmap('my_colors')
    norm = colors.Normalize(vmin=0, vmax=len(array_temps)-1)
    scalar_map = mplcm.ScalarMappable(norm=norm, cmap=cmap)

    for idx, _ in enumerate(array_temps):
        scan = TopticaDLCDLpro(detector=detector,
                               temp=array_temps[idx],
                               polarisation='blue')
        data = scan.get_data(calibrate_intensity=True, calibrate_piezo=True)

        color_val = scalar_map.to_rgba(num_colors[idx])
        lambda_zero = numpy.argmax(data['piezo'] > constants.K_D1)
        ax1.plot(data['piezo'][0:lambda_zero]*1e9, data['intensity'][0:lambda_zero],
                 color=color_val, label=r'{}\,C'.format(array_temps[idx]))
        integral = numpy.sum(data['intensity'][0:lambda_zero])

        scan = TopticaDLCDLpro(detector=detector,
                               temp=array_temps[idx],
                               polarisation='red')
        data = scan.get_data(calibrate_intensity=True, calibrate_piezo=True)

        color_val = scalar_map.to_rgba(num_colors[idx])
        lambda_zero = numpy.argmax(data['piezo'] > constants.K_D1)
        ax1.plot(data['piezo'][lambda_zero:]*1e9, data['intensity'][lambda_zero:],
                 color=color_val)
        integral += numpy.sum(data['intensity'][lambda_zero:])

        print('{} integral: {}'.format(array_temps[idx], integral))

    ax1.set_xlim([769.882, 769.913])
    plt.gca().set_ylim(bottom=0)

    ax1.set_xlabel('Wavelength (nm)')
    ax1.set_ylabel('Intensity (arb. unit)')

    plt.legend(loc='lower center', bbox_to_anchor=[0.5, -0.43],
               ncol=4, shadow=False, title=None, fancybox=False)

    plot.save(fig, '../paper/{}.pdf'.format(detector), tight=False)

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':

    print('Plotting scattering ...')
    main('scatter', [50, 60, 70, 80, 90, 100, 120, 130])
    print('Plotting transmission ...')
    main('transmit', [50, 60, 70, 80, 90, 100, 120, 130])

########################################################################
