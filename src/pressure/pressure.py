#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  PRESSURE.PY
#
#    Steven Hale
#    2017 August 13
#    Birmingham, UK
#

"""Fits vapour pressure coefficients to data.

.. image:: images/vapour_pressure.png
   :width: 600
   :alt: Plot of vapour pressure data and model.

"""

import numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
from   scipy.optimize import curve_fit

import core.plot as plot
import core.constants as constants

########################################################################
########################################################################
########################################################################

def mmhg_to_pa(mmhg):

    """Convert pressure in mmHg to pascals.

    :param mmhg: Pressure in mmHg.
    :type mmhg: float

    :returns: Pressure in pascals.
    :rtype: float

    """

    # 1 mm of mercury = 133.322368 pascals
    return mmhg * 133.322368

def atm_to_pa(atm):

    """Convert pressure in atmospheres to pascals.

    :param atm: Pressure in atmospheres.
    :type atm: float

    :returns: Pressure in pascals.
    :rtype: float

    """

    # 1 atmospheres = 101325 pascals
    return atm * 101325

########################################################################

def edmondson():

    """These values are taken from Edmondson and Egerton (1927), `The
    Vapour Pressures and Melting Points of Sodium and Potassium
    <http://rspa.royalsocietypublishing.org/content/113/765/520>`_.
    The data are used to fit a general model of potassium vapour
    pressure in function :py:meth:`Pressure.pressure.model` and
    :py:meth:`Pressure.pressure.fit`.

    :returns: A 2-tuple.  The first element is the temperature in
              Kelvin.  The second element is the vapour pressure in
              pascals.
    :rtype: tuple

    .. code-block:: bibtex

      @article{520.full,
         author = {Edmondson, W. and Egerton, A.},
          title = {The Vapour Pressures and Melting Points of Sodium and Potassium},
         volume = {113},
         number = {765},
          pages = {520--533},
           year = {1927},
            doi = {10.1098/rspa.1927.0005},
      publisher = {The Royal Society},
           issn = {0950-1207},
        journal = {Proceedings of the Royal Society of London A: Mathematical,
                   Physical and Engineering Sciences}
      }

    """

    # Temperatures are in degrees celsius.
    temp = numpy.array([
        200.6,
        176.3,
        176.1,
        174.4,
        163.5,
        163.4,
        162.7,
        150.8,
        136.5,
        99.5], dtype=numpy.float64)

    temp += constants.KELVIN

    # Pressures are in millimetres of mercury (Torr).
    pressure = numpy.array([
        6.869e-3,
        2.035e-3,
        2.077e-3,
        1.870e-3,
        1.074e-3,
        1.012e-3,
        1.006e-3,
        5.148e-4,
        2.205e-4,
        1.797e-5], dtype=numpy.float64)

    return temp, mmhg_to_pa(pressure)

########################################################################

def hicks():

    """These values are taken from Hicks (1963), `Evaluation of
    Vapor-Pressure Data for Mercury, Lithium, Sodium, and Potassium
    <http://scitation.aip.org/content/aip/journal/jcp/38/8/10.1063/1.1733889>`_.
    The data are used to fit a general model of potassium vapour
    pressure in function :py:meth:`Pressure.pressure.model` and
    :py:meth:`Pressure.pressure.fit`.

    :returns: A 2-tuple.  The first element is the temperature in
              Kelvin.  The second element is the vapour pressure in
              pascals.
    :rtype: tuple

    .. code-block:: bibtex

      @article{1.1733889,
         author = {Hicks, W. T.},
          title = {Evaluation of Vapor-Pressure Data for Mercury, Lithium, Sodium, and Potassium},
        journal = {The Journal of Chemical Physics},
           year = {1963},
         volume = {38},
         number = {8},
          pages = {1873--1880},
            url = {http://scitation.aip.org/content/aip/journal/jcp/38/8/10.1063/1.1733889},
            doi = {http://dx.doi.org/10.1063/1.1733889}
      }

    """

    # Temperatures are in Kelvin.
    temp = numpy.array([
        298.15,
        312.00,
        320.00,
        335.00,
        336.40,
        361.00,
        393.00,
        400.00,
        430.00,
        476.00,
        500.00,
        533.00,
        600.00,
        606.00,
        700.00,
        703.00,
        800.00,
        836.00,
        900.00,
        1000.00,
        1031.00,
        1100.00,
        1200.00,
        1300.00], dtype=numpy.float64)

    # Pressures are in atmospheres.
    pressure = numpy.array([
        1.95e-11,
        1.00e-10,
        2.29e-10,
        1.00e-9,
        1.17e-9,
        1.00e-8,
        1.00e-7,
        1.61e-7,
        1.00e-6,
        1.00e-5,
        2.79e-5,
        1.00e-4,
        8.36e-4,
        1.00e-3,
        9.37e-3,
        1.00e-2,
        5.74e-2,
        1.00e-1,
        2.37e-1,
        7.37e-1,
        1.00,
        1.85,
        3.92,
        7.26], dtype=numpy.float64)

    return temp, atm_to_pa(pressure)

########################################################################

def model(temp, coeff0, coeff1, coeff2, coeff3):

    """General model of potassium vapour pressure.  The coefficients are
    determined in :py:meth:`Pressure.pressure.fit` by
    :obj:`scipy.optimize.curve_fit` using data from
    :py:meth:`Pressure.pressure.edmondson` and
    :py:meth:`Pressure.pressure.hicks`.

    .. math::
      P = a t^3 + b t^2 + c t + d

    The coefficients below as determined by
    :py:meth:`Pressure.pressure.fit` are used in
    :py:meth:`lib.opticaldepth.calc_vapour_pressure` for subsequent
    modelling of optical depth.

    ===========   =================   ================
    Coefficient   Value               Uncertainty
    -----------   -----------------   ----------------
    coeff0         6.0054211235e-07   8.7468694167e-08
    coeff1        -8.9121318942e-04   1.0072983768e-04
    coeff2         4.8897499745e-01   3.8399171195e-02
    coeff3        -9.5551204080e+01   4.8436258495e+00
    ===========   =================   ================

    :param temp: Vapour temperature :math:`t` in Kelvin.
    :type temp: float

    :param coeff0: Model coefficient :math:`a`.
    :type coeff0: float
    :param coeff1: Model coefficient :math:`b`.
    :type coeff1: float
    :param coeff2: Model coefficient :math:`c`.
    :type coeff2: float
    :param coeff3: Model coefficient :math:`d`.
    :type coeff3: float

    :returns: Pressure :math:`P` in pascals.
    :rtype: float

    """

    return (coeff0*temp**3 +
            coeff1*temp**2 +
            coeff2*temp**1 +
            coeff3)

def fit():

    """Fit a function :py:meth:`model` to the potassium vapour pressure
    data measured by :py:meth:`edmondson` and :py:meth:`hicks` .

    :returns: A 2-tuple.  The first element is the temperature in
              Kelvin.  The second element is the vapour pressure in
              pascals.
    :rtype: tuple

    """

    # Edmondson and Egerton (1927)
    ee_temp, ee_pressure = edmondson()
    # Select range for fitting.
    ee_temp = ee_temp[1:]
    ee_pressure = ee_pressure[1:]

    # Hicks (1963)
    hicks_temp, hicks_pressure = hicks()
    # Select range for fitting.
    hicks_temp = hicks_temp[1:9]
    hicks_pressure = hicks_pressure[1:9]

    # Combine both together for fitting.
    temp = numpy.concatenate((ee_temp, hicks_temp), axis=0)
    pressure = numpy.concatenate((ee_pressure, hicks_pressure), axis=0)

    p0 = [1, 1, 1, 1]
    popt, pcov = curve_fit(model, temp, numpy.log(pressure), p0)
    perr = numpy.sqrt(numpy.diag(pcov))

    print('curve_fit: ')
    print("{:.10e} +/- {:.10e}".format(popt[0], perr[0]))
    print("{:.10e} +/- {:.10e}".format(popt[1], perr[1]))
    print("{:.10e} +/- {:.10e}".format(popt[2], perr[2]))
    print("{:.10e} +/- {:.10e}".format(popt[3], perr[3]))

    return popt

def main():

    """Plot the data and the model."""

    plot.set_latex()
    plot.set_fonts()

    # Create a canvas.
    plot.canvas('normal', aspect=4/3)

    ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
    ax2 = plt.subplot2grid((3, 1), (2, 0), sharex=ax1)

    temp_begin = 30
    temp_end = 180

    plot.confax_top(ax1)
    ax1.set_xlim([temp_begin, temp_end])
    ax1.set_ylim([1e-6, 1e1])

    ax1.set_yscale('log')
    ax1.yaxis.set_major_locator(mpl.ticker.LogLocator(base=10.0,
                                                      numticks=8))
    ax1.yaxis.set_minor_locator(mpl.ticker.LogLocator(base=10.0,
                                                      subs=(0.2, 0.4, 0.6, 0.8),
                                                      numticks=8))
    ax1.yaxis.set_minor_formatter(mpl.ticker.NullFormatter())
    ax1.set_ylabel('Vapour Pressure (Pa)')

    plot.confax(ax2)
    ax2.set_ylim([95, 105])
    ax2.set_xlabel('Temperature (C)')
    ax2.set_ylabel('Residual\n Percent')

    ee_temp, ee_pressure = edmondson()
    hicks_temp, hicks_pressure = hicks()

    ax1.scatter(ee_temp - constants.KELVIN, ee_pressure, label=r'E\&E',
                color='b', marker='x', s=20, zorder=1)

    ax1.scatter(hicks_temp - constants.KELVIN, hicks_pressure, label='Hicks',
                color='g', s=20, zorder=1)

    fit_temp = numpy.arange(temp_begin + constants.KELVIN,
                            temp_end + constants.KELVIN)

    popt = fit()

    fit_pressure = numpy.exp(model(fit_temp, *popt))

    ax1.plot(fit_temp - constants.KELVIN, fit_pressure, label='Fit',
             color='k', zorder=0)

    temp = numpy.concatenate((ee_temp, hicks_temp), axis=0)
    pressure = numpy.concatenate((ee_pressure, hicks_pressure), axis=0)

    residuals = 100 * (numpy.exp(model(temp, *popt)) / pressure)

    ax2.scatter(temp - constants.KELVIN, residuals, color='k', s=10)

    ax1.legend(loc='lower right')

    # The figure is finished.
    plot.save(plt, '../paper/vapour_pressure.pdf')

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
