#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  OPTICALDEPTH.PY
#
#    Steven Hale
#    2015 August 13
#    Birmingham, UK
#

"""This Python module calculates the optical depth of potassium."""

import numpy
import core.constants as constants

########################################################################

def calc_vapour_pressure(temp):

    """
    Return the vapour pressure for the given temperature, based on
    previously fitted model coefficients.
    """

    # Coefficients:
    #  6.0054211235e-07 +/- 8.7468694167e-08
    # -8.9121318942e-04 +/- 1.0072983768e-04
    #  4.8897499745e-01 +/- 3.8399171195e-02
    # -9.5551204080e+01 +/- 4.8436258495e+00

    pressure = 6.0054211235e-07 * temp**3
    pressure += -8.9121318942e-04 * temp**2
    pressure += 4.8897499745e-01 * temp
    pressure += -9.5551204080e+01

    return numpy.exp(pressure)

########################################################################

def calc_number_density(cell_temp):

    """
    Calculate the number density of potassium atoms based on the
    vapour pressure at the given temperature.
    """

    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    # Returns: Number of atoms per cubic metre.

    # Model of vapour pressure against temperature.
    pressure = calc_vapour_pressure(cell_temp)
    # Ideal gas law pV = NKT (n = p/KT)
    return pressure / (constants.KB * cell_temp)

########################################################################

def calc_delta_nu(nu0, cell_temp):

    """
    Calculate the FWHM due to Doppler broadening in terms of frequency.
    """

    # nu0: central frequency
    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    #
    # Returns: FWHM in frequency.

    mass = 39.0983 * constants.U # Weight of potassium.

    doppler = 8 * constants.KB * cell_temp * numpy.log(2)
    doppler = doppler / (mass * constants.C**2)
    return nu0 * numpy.sqrt(doppler)

########################################################################

def calc_kappa0(lambda0, delta_nu, cell_temp):

    """
    Calculate k0, the maximum absorption coefficient when Doppler
    broadening alone is present.  For more information see page 100
    of Mitchell and Zemansky (1961).
    """

    # @book{mitchell1961resonance,
    #   title={Resonance Radiation and Excited Atoms},
    #   author={Mitchell, A.C.G. and Zemansky, M.W.},
    #   lccn={lc73030757},
    #   series={The Cambridge series of physical chemistry},
    #   url={https://books.google.co.uk/books?id=Ic7vAAAAMAAJ},
    #   year={1961},
    #   publisher={University Press}
    # }
    #
    # lambda0: Wavelength in metres of the line centre.
    # delta_nu: The FWHM due to Doppler broadening in terms of frequency.
    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    #
    # Returns: Maximum absorption coefficient in m^-1.

    weight_g1 = 1     # Statistical weight of the normal state.
    weight_g2 = 1     # Statistical weight of the excited state.
    lifetime = 27e-9  # State lifetime in seconds.

    number_density = calc_number_density(cell_temp) # Number density.

    natural = (lambda0**2 * weight_g2) / (8 * numpy.pi * weight_g1)
    natural = natural * (number_density / lifetime)

    doppler = 2 / delta_nu
    doppler = doppler * (numpy.sqrt(numpy.log(2)/numpy.pi))

    kappa0 = doppler * natural

    return kappa0

########################################################################

def calc_kappa_lambda(array_lambda,
                      cell_temp,
                      broadening=1.0,
                      lambda0=constants.K_D1):

    """
    Calculate k_lambda, the wavelength-specific absorption
    coefficient when Doppler broadening alone is present.  For more
    information see page 99 of Mitchell and Zemansky (1961).
    """

    # array_lambda: Wavelength in metres.
    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    # broadening: Broadening multiplier due to other factors, such as
    #             non-uniform magnetic field in Zeeman split components.
    #
    # Returns: Absorption coefficient in m^-1.

    nu0 = constants.C / lambda0
    array_nu = constants.C / array_lambda

    delta_nu = calc_delta_nu(nu0, cell_temp)
    delta_nu = delta_nu * broadening
    kappa0 = calc_kappa0(lambda0, delta_nu, cell_temp)

    exp = 2 * (array_nu - nu0) * numpy.sqrt(numpy.log(2))
    exp = exp / delta_nu
    exp = exp**2

    k_nu = kappa0 * numpy.exp(-exp)

    return k_nu

########################################################################

def calc_tau_lambda(array_lambda,
                    cell_temp,
                    cell_length,
                    broadening=1.0,
                    lambda0=constants.K_D1):

    """
    Calculate the optical depth per wavelength after
    absorption through a distance S of potassium vapour at
    temperature Temp.
    """

    # array_lambda: Array of wavelengths in nanometres.
    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    # cell_length: Thickness of material in metres.
    # broadening: Broadening multiplier due to other factors, such as
    #             non-uniform magnetic field in Zeeman split components.
    #
    # Returns: Optical depth Tau.

    kappa_lambda = calc_kappa_lambda(array_lambda, cell_temp, broadening, lambda0)
    tau_lambda = kappa_lambda * cell_length

    return tau_lambda

########################################################################

def calc_intensity_lambda(array_lambda,
                          cell_temp,
                          cell_length,
                          broadening=1.0,
                          lambda0=constants.K_D1):

    """
    Calculate the transmitted intensity per wavelength after absorption
    through a distance cell_length of potassium vapour at temperature
    cell_temp.
    """

    # array_lambda: Array of wavelengths in nanometres.
    # cell_temp: Vapour temperature in Kelvin from 300-533K.
    # cell_length: Thickness of material in millimetres.
    # broadening: Broadening multiplier due to other factors, such as
    #             non-uniform magnetic field in Zeeman split components.
    # lambda0: Central wavelength, default 769.898nm
    #
    # Returns: Intensity I0 minus absorption.

    intensity_lambda = calc_tau_lambda(array_lambda,
                                       cell_temp,
                                       cell_length,
                                       broadening=broadening,
                                       lambda0=lambda0)

    return numpy.exp(-intensity_lambda)

########################################################################
