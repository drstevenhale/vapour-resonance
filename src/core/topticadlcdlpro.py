#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  TOPTICADLCDLPRO.PY
#
#    Steven Hale
#    2016 October 28
#    Birmingham, UK
#

"""This module reads .csv data files from a Toptica DLC DLpro laser.

"""

import numpy
import core.zeeman as zeeman
import core.constants as constants

########################################################################
########################################################################
########################################################################

class TopticaDLCDLpro():

    """This class reads .csv data files from a Toptica DLC DLpro laser.

    """

    def __init__(self, filespec=None,
                 detector='transmit',
                 temp='90',
                 polarisation='blue'):

        """Initialise an instance of TopticaDLC100."""

        self.detector = None
        self.data = None
        self.datadir = '../data'

        self.detector = detector
        self.temp = temp
        self.polarisation = polarisation

        if filespec is None:
            self.filespec = '{}/{}_{}_{}.csv'.format(self.datadir,
                                                     self.polarisation,
                                                     self.temp,
                                                     self.detector)
        else:
            self.filespec = '{}/{}'.format(self.datadir, filespec)

        self.read_file()
        self.crop_range()

########################################################################

    def read_file(self):

        """This loads a .csv file from a Toptica DLC100 laser.  There are
        three columns, delimited by semi-colons.  The first row
        describes the column names, and so we use skiprows=1.
        Unfortunately, each row has a trailing semi-colon and this
        causes numpy.loadtxt to think there is a missing value,
        causing it to choke.  We need to provide usecols=range(3) to
        tell it explicitly to expect 3 columns.

        """

        data = numpy.loadtxt(self.filespec,
                             delimiter=';',
                             skiprows=1,
                             usecols=range(3))

        # Data from the scattering detector is in the 2nd column.
        # from the transmission detector is in the 3rd column.

        self.data = {'piezo' : data[:, 0],
                     'scatter' : data[:, 1],
                     'transmit' : data[:, 2]}

########################################################################

    def crop_range(self):

        """There is a mode hop towards the end of the scan range.  We need to
        crop it out to prevent it interfering with the subsequent
        analysis.

        """

        crop = -37
        self.data['piezo'] = self.data['piezo'][0:crop]
        self.data['scatter'] = self.data['scatter'][0:crop]
        self.data['transmit'] = self.data['transmit'][0:crop]

########################################################################

    def calibrate_intensity(self):

        """Remove dark voltage and calibrate flat field."""

        #   Scattering:   0.040348 V
        # Transmission:   0.008967 V

        if self.detector == 'scatter':
            dark = 0.040348

        if self.detector == 'transmit':
            #dark = 0.008967
            dark = 0.119

        scan = TopticaDLCDLpro(detector='transmit',
                               temp='cold',
                               polarisation='blue')
        flat_b = scan.get_data()
        scan = TopticaDLCDLpro(detector='transmit',
                               temp='cold',
                               polarisation='red')
        flat_r = scan.get_data()
        flat_r['intensity'] = (flat_r['intensity'] + flat_b['intensity'])/2
        flat_r['intensity'] -= dark

        self.data['intensity'] -= dark
        self.data['intensity'] = self.data['intensity'] / flat_r['intensity']

########################################################################

    def calibrate_piezo(self):

        """Convert piezo voltage to wavelength."""

        red_component = 72.213
        blue_component = 92.899
        mean_mag = 0.283
        lambda0 = constants.K_D1

        # Expected magnetic splitting.
        delta_nu = zeeman.b_to_deltanu(mean_mag)
        delta_lambda = zeeman.deltanu_to_lambda(delta_nu, lambda0)

        # Calibrate the piezo voltage to put 0.0 at the center of the
        # blue component, and 1.0 at the center of the red
        # component.
        self.data['piezo'] = ((self.data['piezo'] - blue_component) /
                              (red_component - blue_component))
        # Now multiply by twice the spliting, to put 0nm at the blue
        # component, and 2*delta_lambda at the red component.
        self.data['piezo'] = 2 * delta_lambda * self.data['piezo']
        # Subtract delta_lambda to center the splitting on zero and
        # return the relative wavelength.
        self.data['piezo'] -= delta_lambda
        # Add lambda0 to return the absolute wavelength.
        self.data['piezo'] += lambda0

        # Wavelength goes down with increasing piezo voltage.  We now
        # need to reverse both arrays.
        self.data['piezo'] = numpy.flip(self.data['piezo'], axis=0)
        self.data['intensity'] = numpy.flip(self.data['intensity'], axis=0)

########################################################################

    def get_data(self, calibrate_intensity=False, calibrate_piezo=False):

        """Acquires the requested data from the file."""

        if self.detector == 'scatter':
            # Data from the scattering detector is in the 2nd column.
            self.data = {'piezo':self.data['piezo'],
                         'intensity':self.data['scatter']}
        elif self.detector == 'transmit':
            # Data from the transmission detector is in the 3rd column.
            self.data = {'piezo':self.data['piezo'],
                         'intensity':self.data['transmit']}
        else:
            raise ValueError('Unknown detector type')

        if calibrate_intensity:
            self.calibrate_intensity()
        if calibrate_piezo:
            self.calibrate_piezo()

        return self.data

########################################################################
