#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  AGILENT86142B.PY
#
#    Steven Hale
#    2015 May 8
#    Birmingham, UK
#

"""Tests for the agilent86142b module."""

from pytest import approx
import core.agilent86142b as agilent86142b

# pylint: disable=attribute-defined-outside-init

########################################################################
########################################################################
########################################################################

class TestInstance():

    """Test we can create an instance of the object."""

    def setup_method(self):
        """Provide a random filename."""
        self.scan = agilent86142b.Agilent86142b('filespec')

    def test_instantiation(self):
        """Test the instance has been created and the filespec set."""
        assert self.scan.filespec == 'filespec'
        assert self.scan.state == 'header'

    def test_start(self):
        """Test the response to a START parameter."""
        self.scan.process_line('START,769.650')
        assert self.scan.start == 769.650

    def test_stop(self):
        """Test the response to a STOP parameter."""
        self.scan.process_line('STOP,770.150')
        assert self.scan.stop == 770.150

    def test_points(self):
        """Test the response to a LENGTH parameter."""
        self.scan.process_line('LENGTH (pts),10001')
        assert self.scan.points == 10001

    def test_reference(self):
        """Test the response to a REFERENCE parameter."""
        self.scan.process_line('REFERENCE LEVEL (dBm),-7.42')
        assert self.scan.reference == -7.42

    def test_lambda_reference(self):
        """Test the response to an AIR/VACUUM/STP parameter."""
        self.scan.process_line('AIR/VACUUM/STP,AIR')
        assert self.scan.lambda_reference == 'AIR'

    def test_sensitivity(self):
        """Test the response to a SENSITIVITY parameter."""
        self.scan.process_line('SENSITIVITY (dBm),-48.73')
        assert self.scan.sensitivity == -48.73

    def test_index(self):
        """Test the response to an INDEX parameter."""
        self.scan.start = 769.650
        self.scan.stop = 770.150
        self.scan.points = 10001
        self.scan.process_line('INDEX,A')
        assert self.scan.index == 0
        assert self.scan.state == 'data'

    def test_done(self):
        """Test the response to a data value line."""
        self.scan.start = 769.650
        self.scan.stop = 770.150
        self.scan.points = 10001
        self.scan.process_line('INDEX,A')
        self.scan.process_line('1,-56.108')
        self.scan.process_line('10001,-32.718')
        assert self.scan.state == 'done'

    def test_unexpected1(self):
        """Test the response to an unexpected parameter."""
        self.scan.process_line('RANDOM')

    def test_unexpected2(self):
        """Test the response to an unexpected system state."""
        self.scan.state = 'random'
        self.scan.process_line('RANDOM')

########################################################################

class TestOutput():

    """Test we can acquire the expected data from a known file."""

    def setup_method(self):
        """Open a known test file."""
        self.scan = agilent86142b.Agilent86142b('../data/laser_pc_70v.csv')
        self.scan.read_file()

    def test_db(self):
        """Test the raw dB values are returned."""
        data = self.scan.get_db()
        assert data['lambda'][0] == 769.650
        assert data['lambda'][-1] == 770.150
        assert data['data'][0] == -56.716
        assert data['data'][-1] == -56.518

    def test_db_movingmean(self):
        """Test the raw dB values are returned after a 25 point moving mean."""
        window = 25
        data = self.scan.get_db_movingmean(window)
        assert data['lambda'][0] == 769.650
        assert data['lambda'][-1] == 770.150
        assert data['data'][0] == approx(-29.53168)
        assert data['data'][-1] == approx(-29.43812)

    def test_linear(self):
        """Test the linear voltage values are returned."""
        data = self.scan.get_linear()
        assert data['lambda'][0] == 769.650
        assert data['lambda'][-1] == 770.150
        assert data['data'][0] == approx(2.1301000374112523e-09)
        assert data['data'][-1] == approx(2.2294616179163875e-09)

    def test_linear_movingmean(self):
        """Test the linear voltage values are returned after a 25 point moving mean."""
        window = 25
        data = self.scan.get_linear_movingmean(window)
        assert data['lambda'][0] == 769.650
        assert data['lambda'][-1] == 770.150
        assert data['data'][0] == approx(1.0889422973293914e-09)
        assert data['data'][-1] == approx(1.1349057675513894e-09)

########################################################################
