#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  ZEEMAN.PY
#
#    Steven Hale
#    2015 August 16
#    Birmingham, UK
#

"""Tests of Zeeman splitting calculations."""

from pytest import approx
import core.zeeman as zeeman

########################################################################
########################################################################
########################################################################

def test_mag_to_dnu():
    """Magnetic field to frequency."""
    mag = 0.3
    delta_nu = 5598498224
    assert zeeman.b_to_deltanu(mag) == approx(delta_nu, abs=1)

def test_dnu_to_mag():
    """Frequency to magnetic field."""
    delta_nu = 5598498224
    mag = 0.3
    assert zeeman.deltanu_to_b(delta_nu) == approx(mag)

########################################################################

def test_dnu_to_dlambda():
    """Frequency shift to wavelength shift."""
    delta_nu = 5598498224
    lambda0 = 770e-9
    delta_lambda = 1.10722e-11
    assert zeeman.deltanu_to_lambda(delta_nu, lambda0) == approx(delta_lambda, abs=1e-16)

########################################################################

def test_dnu_to_doppler():
    """Frequency shift to Doppler shift."""
    mag = 0.18 # T
    delta_nu = zeeman.b_to_deltanu(mag)
    lambda0 = 770e-9 # nm
    delta_doppler = 2586.5 # m/s
    assert zeeman.deltanu_to_doppler(delta_nu, lambda0) == approx(delta_doppler, abs=0.1)

########################################################################
