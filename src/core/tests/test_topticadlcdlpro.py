#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  TOPTICADLCDLPRO.PY
#
#    Steven Hale
#    2016 October 28
#    Birmingham, UK
#

"""Tests for the topticadlcdlpro module."""

import pytest
import core.topticadlcdlpro as dlc

# pylint: disable=attribute-defined-outside-init
# pylint: disable=too-few-public-methods

########################################################################
########################################################################
########################################################################

class TestInstance():

    """Test we can create an instance of the object."""

    def test_instantiation(self):
        """Check we can read expected values."""
        self.scan = dlc.TopticaDLCDLpro('blue_cold_transmit.csv')
        assert self.scan.data['piezo'][0] == 67.032463
        assert self.scan.data['scatter'][0] == 0.594940
        assert self.scan.data['transmit'][0] == 3.753180
        assert self.scan.data['piezo'][-1] == 98.793259
        assert self.scan.data['scatter'][-1] == 0.285895
        assert self.scan.data['transmit'][-1] == 1.649798

########################################################################

class TestBadInput():

    """Test bad input."""

    def test_bad_detector(self):
        """Check file not found."""
        with pytest.raises(OSError):
            self.scan = dlc.TopticaDLCDLpro(detector='foo',
                                            temp='90',
                                            polarisation='blue')
    def test_missing_data(self):
        """Check file not found."""
        self.scan = dlc.TopticaDLCDLpro(detector='scatter',
                                        temp='90',
                                        polarisation='blue')
        self.scan.detector = 'foo'
        with pytest.raises(ValueError):
            self.scan.get_data()

########################################################################

class TestOutputTransmit():

    """Test we can acquire the expected data from a known file."""

    def setup_method(self):
        """Provide a random filename."""
        self.scan = dlc.TopticaDLCDLpro(detector='transmit',
                                        temp='90',
                                        polarisation='blue')

    def test_raw(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=False, calibrate_piezo=False)
        assert data['piezo'][0] == 67.032585
        assert data['piezo'][1] == 67.065002
        assert data['piezo'][2] == 67.097412
        assert data['intensity'][0] == 3.727634
        assert data['intensity'][1] == 3.72464
        assert data['intensity'][2] == 3.722595

    def test_calibrate_intensity(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=True, calibrate_piezo=False)
        assert data['piezo'][0] == 67.032585
        assert data['piezo'][-1] == 98.793396
        assert data['intensity'][0] == 0.9919097146918955
        assert data['intensity'][-1] == 1.0036721200194505

    def test_calibrate_piezo(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=True, calibrate_piezo=True)
        assert data['piezo'][0] == 769.8816072331033e-9
        assert data['piezo'][-1] == 769.9136719540211e-9
        assert data['intensity'][0] == 1.0036721200194505
        assert data['intensity'][-1] == 0.9919097146918955

########################################################################

class TestOutputScatter():

    """Test we can acquire the expected data from a known file."""

    def setup_method(self):
        """Provide a random filename."""
        self.scan = dlc.TopticaDLCDLpro(detector='scatter',
                                        temp='90',
                                        polarisation='blue')

    def test_raw(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=False, calibrate_piezo=False)
        assert data['piezo'][0] == 67.032532
        assert data['piezo'][1] == 67.064934
        assert data['piezo'][2] == 67.097351
        assert data['intensity'][0] == 0.047726
        assert data['intensity'][1] == 0.043099
        assert data['intensity'][2] == 0.042869

    def test_calibrate_intensity(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=True, calibrate_piezo=False)
        assert data['piezo'][0] == 67.032532
        assert data['piezo'][-1] == 98.79332
        assert data['intensity'][0] == 0.0019850841562141217
        assert data['intensity'][-1] == -0.0008877923584048797

    def test_calibrate_piezo(self):
        """Check we can read expected values."""
        data = self.scan.get_data(calibrate_intensity=True, calibrate_piezo=True)
        assert data['piezo'][0] == 769.8816073098305e-9
        assert data['piezo'][-1] == 769.9136720075283e-9
        assert data['intensity'][0] == -0.0008877923584048797
        assert data['intensity'][-1] == 0.0019850841562141217

########################################################################
