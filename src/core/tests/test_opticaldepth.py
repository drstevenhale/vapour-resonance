#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  OPTICALDEPTH.PY
#
#    Steven Hale
#    2015 August 13
#    Birmingham, UK
#

"""Tests for the optical depth module."""

import pytest
import core.opticaldepth as opticaldepth
import core.constants as constants

# pylint: disable=attribute-defined-outside-init

########################################################################
########################################################################
########################################################################

class TestOpticalDepth():

    """Tests for the optical depth module."""

    # Test vapour temperatures.
    temp = [40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0, 110.0, 120.0]
    temp = [x+constants.KELVIN for x in temp]

    # Potassium vapour pressure expected results.
    pressure = [0.001139924e-2,
                0.00323383e-2,
                0.00862419e-2,
                0.02169916e-2,
                0.05169593e-2,
                0.11703708e-2,
                0.25270230e-2,
                0.52224985e-2,
                1.03680182e-2]

    @staticmethod
    @pytest.mark.parametrize("temp,pressure", list(zip(temp, pressure)))
    def test_calc_vapour_pressure(temp, pressure):
        """Test expected pressures are returned."""
        assert opticaldepth.calc_vapour_pressure(temp) == pytest.approx(pressure)

    # Potassium number density expected results.
    number_density = [2.636576e+15,
                      7.2482e+15,
                      1.874974e+16,
                      4.58011e+16,
                      1.060264e+17,
                      2.334286e+17,
                      4.90504e+17,
                      9.87248e+17,
                      1.91009e+18]

    @staticmethod
    @pytest.mark.parametrize("temp,number_density", list(zip(temp, number_density)))
    def test_calc_number_density(temp, number_density):
        """Test expected number densities are returned."""
        assert opticaldepth.calc_number_density(temp) == pytest.approx(number_density)

    # Potassium FWHM due to Doppler broadending expected results.
    delta_nu = [789294149,
                801797606,
                814109053,
                826237071,
                838189624,
                849974114,
                861597437,
                873066029,
                884385911]

    @staticmethod
    @pytest.mark.parametrize("temp,delta_nu", list(zip(temp, delta_nu)))
    def test_delta_nu(temp, delta_nu):
        """Test expected Doppler FWHMs are returned."""
        nu0 = constants.C / constants.K_D1
        assert opticaldepth.calc_delta_nu(nu0, temp) == pytest.approx(delta_nu)

    # Maximum absorption coefficient expected results.
    kappa0_results = [2.7411451145166956,
                      7.418157149358618,
                      18.899191715220834,
                      45.48849633522171,
                      103.80116155865242,
                      225.3611691796373,
                      467.16326114639486,
                      927.9182766242966,
                      1772.3234444311]

    @staticmethod
    @pytest.mark.parametrize("temp,kappa0_results", list(zip(temp, kappa0_results)))
    def test_kappa0(temp, kappa0_results):
        """Test expected absorption coefficients are returned."""
        lambda0 = constants.K_D1
        nu0 = constants.C / constants.K_D1
        delta_nu = opticaldepth.calc_delta_nu(nu0, temp)
        kappa0 = opticaldepth.calc_kappa0(lambda0, delta_nu, temp)
        assert kappa0 == pytest.approx(kappa0_results)

    @staticmethod
    @pytest.mark.parametrize("temp,kappa0_results", list(zip(temp, kappa0_results)))
    def test_kappa_lambda(temp, kappa0_results):
        """Test expected absorption coefficients are returned."""
        kappa_lambda = opticaldepth.calc_kappa_lambda(constants.K_D1, temp)
        assert kappa_lambda == pytest.approx(kappa0_results)

    @staticmethod
    @pytest.mark.parametrize("temp,kappa0_results", list(zip(temp, kappa0_results)))
    def test_tau_lambda(temp, kappa0_results):
        """Test expected absorption coefficients are returned."""
        cell_length = 15e-3
        tau_lambda = opticaldepth.calc_tau_lambda(constants.K_D1,
                                                  temp,
                                                  cell_length)
        assert tau_lambda == pytest.approx(kappa0_results * cell_length)

    # Maximum absorption coefficient expected results.
    tau_results = [0.9597166669099039,
                   0.894695039070643,
                   0.7531522535072523,
                   0.5054392402737253,
                   0.21076375372409167,
                   0.03403324160630874,
                   0.0009051146681108281,
                   9.018892119067709e-07,
                   2.8467256796168353e-12]

    @staticmethod
    @pytest.mark.parametrize("temp,tau_results", list(zip(temp, tau_results)))
    def test_intensity_lambda(temp, tau_results):
        """Test expected absorption coefficients are returned."""
        cell_length = 15e-3
        tau_lambda = opticaldepth.calc_intensity_lambda(constants.K_D1, temp, cell_length)
        assert tau_lambda == pytest.approx(tau_results)

########################################################################
