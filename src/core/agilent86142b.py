#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  AGILENT86142B.PY
#
#    Steven Hale
#    2015 May 8
#    Birmingham, UK
#

"""This module reads .csv data files from an Agilent 86142B optical
spectrum analyser.

"""

import numpy

########################################################################
########################################################################
########################################################################

class Agilent86142b():

    """This class reads .csv data files from an Agilent 86142B optical
    spectrum analyser.

    """

    def __init__(self, filespec):

        """Initialise an instance of Agilent86142b."""

        self.start = None
        self.stop = None
        self.points = None
        self.reference = None
        self.lambda_reference = None
        self.sensitivity = None
        self.array_lambda = None
        self.array_data = None
        self.index = None

        self.state = "header"
        self.filespec = filespec

########################################################################

    def process_line(self, line):

        """Process each line in the file.  We have to keep track of file
        state, to know if we have finished reading the header and have
        got to the data yet."""

        line = line.strip()

        if self.state == "header":

            if line.startswith("START"):
                line = line.split(',')
                self.start = float(line[1])

            elif line.startswith("STOP"):
                line = line.split(',')
                self.stop = float(line[1])

            elif line.startswith("LENGTH"):
                line = line.split(',')
                self.points = float(line[1])

            elif line.startswith("REFERENCE"):
                line = line.split(',')
                self.reference = float(line[1])

            elif line.startswith("AIR/VACUUM/STP"):
                line = line.split(',')
                self.lambda_reference = line[1]

            elif line.startswith("SENSITIVITY"):
                line = line.split(',')
                self.sensitivity = float(line[1])

            elif line.startswith("INDEX"):
                delta_lambda = (self.stop - self.start) / (self.points - 1)
                self.array_lambda = self.start + numpy.arange(self.points) * delta_lambda
                self.array_data = numpy.zeros(int(self.points))
                self.index = 0
                self.state = "data"

            else:
                return

        elif self.state == "data":

            line = line.split(',')
            self.array_data[self.index] = float(line[1])
            self.index += 1
            if int(line[0]) == self.points:
                self.state = "done"

        else:
            return

########################################################################

    def read_file(self):

        """Read the whole file, processing each line in turn."""

        with open(self.filespec, mode='r', encoding='UTF-8') as fd:
            array_data = fd.readlines()

        for line in array_data:
            self.process_line(line)

########################################################################

    def get_db(self):
        """Return raw dB intensity values."""
        return {'lambda':self.array_lambda, 'data':self.array_data}

    def get_db_movingmean(self, window):
        """Return raw dB intensity values after a moving mean."""
        window = numpy.ones(int(window))/float(window)
        return {'lambda':self.array_lambda,
                'data':numpy.convolve(self.array_data, window, mode='same')}

########################################################################

    def get_linear(self):
        """Return linear voltage intensity values."""
        array_data = 10 ** (self.array_data / 10) / 1000 # W nm^-1
        return {'lambda':self.array_lambda, 'data':array_data}

    def get_linear_movingmean(self, window):
        """Return linear voltage intensity values after a moving mean."""
        window = numpy.ones(int(window))/float(window)
        data = self.get_linear()
        return {'lambda':data['lambda'], 'data':numpy.convolve(data['data'], window, mode='same')}

########################################################################
