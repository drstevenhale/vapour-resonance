#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  ZEEMAN.PY
#
#    Steven Hale
#    2015 August 16
#    Birmingham, UK
#

"""This Python module calculates Zeeman splitting."""

import core.constants as constants

########################################################################
########################################################################
########################################################################

def b_to_deltanu(mag):
    """From B to splitting, in frequency."""
    delta_nu = (4 * constants.MUB * mag) / (3 * constants.H)
    return delta_nu

def deltanu_to_b(delta_nu):
    """From splitting in frequency, to B."""
    mag = (3 * constants.H * delta_nu) / (4 * constants.MUB)
    return mag

def deltanu_to_lambda(delta_nu, lambda0):
    """From frequency shift to wavelength shift."""
    delta_lambda = (delta_nu * lambda0**2) / constants.C
    return delta_lambda

def deltanu_to_doppler(delta_nu, lambda0):
    """From frequency shift to wavelength shift."""
    delta_lambda = deltanu_to_lambda(delta_nu, lambda0)
    delta_doppler = (delta_lambda * constants.C) / lambda0
    return delta_doppler

########################################################################
