#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  CONSTANTS.PY
#
#    Steven Hale
#    2015 August 13
#    Birmingham, UK
#

"""This module provides some handy physical constants."""

# Constants
C      = 2.99792458e+8  # m s^-1           (Speed of light)
H      = 6.62606957e-34 # m^2 kg s^-1      (Planck constant, Joule second)
HBAR   = 6.58211928e-16 # eV⋅s             (h/2 pi in electron volts)
KB     = 1.3806488e-23  # m^2 kg s^-2 K^-1 (Boltzmann constant, Joules per Kelvin)
SB     = 5.67037321e-8  # W m^-2 K^-4      (Stefan-Boltzmann constant)
U      = 1.66053892e-27 # kg               (Unified atomic mass unit)
MUB    = 9.27400968e-24 # J·T-1            (Bohr magneton)
K      = 39.0983        # u                (Atomic weight of potassium)
K_D1   = 769.898e-9     # m                (Wavelength of D1 transition)

# Conversions
KELVIN = 273.15         # Kelvin
MM2IN  = 0.0393701      # 1mm in inches
PC2IN  = 400 / 2409     # LaTeX 1pc to inches
