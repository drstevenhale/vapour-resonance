#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  CALIBRATION_COARSE.PY
#
#    Steven Hale
#    2017 August 28
#    Birmingham, UK
#

"""Plots the laser wavelength over the whole piezo range.

.. image:: images/coarse.png
   :width: 600
   :alt: Plot of coarse laser calibration.

"""

import matplotlib.pyplot as plt
import core.plot as plot
from core.agilent86142b import Agilent86142b

########################################################################
########################################################################
########################################################################

def main():

    """Plot three data files from the optical spectrum analyser.  These
    show the extent of the fine tuning range of the laser.

    """

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('normal')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1)

    ax1.set_xlabel('Wavelength (nm)')
    ax1.set_ylabel('Intensity (arb. unit)')

    # Draw laser with piezo at 0V.

    scan = Agilent86142b('../data/laser_pc_0v.csv')
    scan.read_file()
    data = scan.get_linear()
    data['data'] = data['data'] / max(data['data'])
    ax1.plot(data['lambda'], data['data'], color='r', label=r'0\,V', zorder=0)

    # Draw laser with piezo at 70V (midrange).

    scan = Agilent86142b('../data/laser_pc_70v.csv')
    scan.read_file()
    data = scan.get_linear()
    data['data'] = data['data'] / max(data['data'])
    ax1.plot(data['lambda'], data['data'], color='k', label=r'70\,V', zorder=1)

    # Draw laser with piezo at 140V.

    scan = Agilent86142b('../data/laser_pc_140v.csv')
    scan.read_file()
    data = scan.get_linear()
    data['data'] = data['data'] / max(data['data'])
    ax1.plot(data['lambda'], data['data'], color='b', label=r'140\,V', zorder=0)

    # Draw a hatched area showing the extent of the range we need to
    # scan.  This needs to be within the fine tuning range of the
    # laser.

    ax1.fill([769.885, 769.911, 769.911, 769.885], [-1, -1, 2, 2],
             fill=False, hatch='\\\\', alpha=0.2, linestyle='dashed')

    ax1.set_xlim([data['lambda'][0], data['lambda'][-1]])
    ax1.set_ylim([0, 1.02])

    plt.legend(loc='lower center', bbox_to_anchor=[0.5, -0.6],
               ncol=4, shadow=False, title=None, fancybox=False)

    plot.save(fig, '../paper/coarse.pdf')

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
