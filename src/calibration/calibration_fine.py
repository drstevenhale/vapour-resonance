#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  CALIBRATION_FINE.PY
#
#    Steven Hale
#    2017 July 14
#    Birmingham, UK
#

"""Calibrates the position of the Zeeman-split absorption profiles.

.. image:: images/calibration1.png
   :width: 600
   :alt: Plot of fine laser calibration.

.. image:: images/calibration2.png
   :width: 600
   :alt: Plot of fine laser calibration.

.. image:: images/calibration3.png
   :width: 600
   :alt: Plot of fine laser calibration.

"""

import numpy
import matplotlib.pyplot as plt

import core.plot as plot
from core.topticadlcdlpro import TopticaDLCDLpro

########################################################################
########################################################################
########################################################################

def make_dark_voltage():

    """Measure the detector dark voltages."""

    print('Detector dark voltages:')
    scan = TopticaDLCDLpro('dark.csv', detector='scatter')
    data = scan.get_data()
    print('  Scattering: {:10.6f} V'.format(numpy.mean(data['intensity'])))
    scan = TopticaDLCDLpro('dark.csv', detector='transmit')
    data = scan.get_data()
    print('Transmission: {:10.6f} V'.format(numpy.mean(data['intensity'])))

    #   Scattering:   0.040348 V
    # Transmission:   0.008967 V

    return numpy.mean(data['intensity'])

########################################################################

def find_fwhm(data):

    """Find the line centre and FWHM of the absorption line."""

    # Find the minimum transmission value.
    trans_min = numpy.min(data['intensity'])
    # The width threshold is half this value.
    trans_thresh = trans_min + ((1.0 - trans_min) / 2)

    # Find the edges.
    left = numpy.argmax(data['intensity'] < trans_thresh)
    right = numpy.argmax(data['intensity'][left:] > trans_thresh) + left

    # Convert to piezo voltage.
    left = data['piezo'][left]
    right = data['piezo'][right]

    fwhm = right - left
    centre = numpy.mean([left, right])

    return centre, fwhm

########################################################################

def get_absorption_line_centre(component):

    """Find the mean line centre the absorption line for both components,
    over a range of temperatures..

    """

    temps = [50, 60, 70, 80, 90, 100, 120]
    mean_centre = []

    for temp in temps:
        scan = TopticaDLCDLpro(detector='transmit', temp=temp, polarisation=component)
        data = scan.get_data(calibrate_intensity=True)
        centre, fwhm = find_fwhm(data)
        mean_centre.append(centre)
        print('Centre: {:7.3f} V   Width: {:5.3f} V'.format(centre, fwhm))

    print('Mean {} centre: {:7.3f} V'.format(component, numpy.mean(mean_centre)))

########################################################################

def plot_calibration(out_filespec, calibrate_intensity, calibrate_piezo):

    """Produce figures demonstrating the laser calibration."""

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('narrow')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1)

    cell_temp = 90

    # Plot cold intensity.
    scan = TopticaDLCDLpro(detector='transmit',
                           temp='cold',
                           polarisation='blue')
    data = scan.get_data(calibrate_intensity=calibrate_intensity, calibrate_piezo=calibrate_piezo)
    if calibrate_piezo:
        data['piezo'] = 1e9 * data['piezo']
    ax1.plot(data['piezo'], data['intensity'],
             color='k', label='Cold', linewidth=4, zorder=0)

    # Plot hot intensity, red component (left circular).
    scan = TopticaDLCDLpro(detector='transmit',
                           temp=cell_temp,
                           polarisation='red')
    data = scan.get_data(calibrate_intensity=calibrate_intensity, calibrate_piezo=calibrate_piezo)
    if calibrate_piezo:
        data['piezo'] = 1e9 * data['piezo']
    ax1.plot(data['piezo'], data['intensity'],
             color='r', label=r'{}\,C Left Circular'.format(cell_temp), zorder=1)

    # Plot hot intensity, blue component (right circular).
    scan = TopticaDLCDLpro(detector='transmit',
                           temp=cell_temp,
                           polarisation='blue')
    data = scan.get_data(calibrate_intensity=calibrate_intensity, calibrate_piezo=calibrate_piezo)
    if calibrate_piezo:
        data['piezo'] = 1e9 * data['piezo']
    ax1.plot(data['piezo'], data['intensity'],
             color='b', label=r'{}\,C Right Circular'.format(cell_temp), zorder=1)

    # Add a dashed vertical line to indicate central wavelength.
    ax1.axvline(x=769.898, linewidth=1, linestyle='dashed', color='#888888')

    if calibrate_piezo:
        ax1.set_xlim([769.881, 769.914])
        ax1.set_xlabel('Wavelength (nm)')
    else:
        ax1.set_xlim([67, 98])
        ax1.set_xlabel('Piezo (volts)')

    if calibrate_intensity:
        ax1.set_ylim([0, 1.0])
        ax1.set_ylabel('Intensity (arb. unit)')
    else:
        ax1.set_ylim([0, 4.0])
        ax1.set_ylabel('Intensity (volts)')

    plt.legend(loc='lower center', bbox_to_anchor=[0.5, -0.5],
               ncol=4, shadow=False, title=None, fancybox=False)

    plot.save(fig, '../paper/{}.pdf'.format(out_filespec), tight=False)

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    make_dark_voltage()
    get_absorption_line_centre('red')
    get_absorption_line_centre('blue')
    plot_calibration('calibration1', calibrate_intensity=False, calibrate_piezo=False)
    plot_calibration('calibration2', calibrate_intensity=True, calibrate_piezo=False)
    plot_calibration('calibration3', calibrate_intensity=True, calibrate_piezo=True)

########################################################################
