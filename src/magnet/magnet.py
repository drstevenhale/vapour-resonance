#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  MAGNET.PY
#
#    Steven Hale
#    2017 August 5
#    Birmingham, UK
#

"""The field strength of the magnet was measured using a Hirst GM04
Gaussmeter.  This file plots the field across the cell.

.. image:: images/magnet.png
   :width: 600
   :alt: Plot of magnetic uniformity.

"""

import numpy
import core.plot as plot

########################################################################
########################################################################
########################################################################

def main():

    """Plot the field across the cell."""

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('narrow')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1)
    #ax1.set_xlim([-1,18])
    ax1.set_ylim([0.25, 0.31])

    ax1.set_xlabel('Position in cell (mm)')
    ax1.set_ylabel('Field strength (T)')

    array_position = [0.0, 4.25, 8.5, 12.75, 17.0]
    array_mag = numpy.array([0.259, 0.289, 0.305, 0.294, 0.268])

    ax1.scatter(array_position, array_mag, color='k')

    ax1.axvline(x=array_position[0], linewidth=1.5, linestyle='dashed', color='k')
    ax1.axvline(x=array_position[2], linewidth=1.5, linestyle='dashed', color='k')
    ax1.axvline(x=array_position[4], linewidth=1.5, linestyle='dashed', color='k')

    mean_mag = numpy.mean(array_mag)

    ax1.text(0.1, 0.9, 'Mean B: {:.3f} T'.format(mean_mag),
             transform=ax1.transAxes, color='k')

    plot.save(fig, '../paper/magnet.pdf')

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
