#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  BISON_CELL.PY
#
#    Steven Hale
#    2015 August 18
#    Birmingham, UK
#

"""Plots the transmission intensity through a vapour cell for various
vapour temperatures.

.. image:: images/bison_cell_transmission.png
   :width: 600
   :alt: Plot of scattering intensity against vapour temperature and
         detector aperture.

"""

import numpy
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.cm as mplcm
import matplotlib.colors as colors

import core.plot as plot
import core.constants as constants
import core.opticaldepth as opticaldepth

########################################################################
########################################################################
########################################################################

def main():

    """
    Plots the transmission intensity through a vapour cell for various
    vapour temperatures.
    """

    # pylint: disable=too-many-locals

    cell_length = 15 # m (17mm cell, 1mm windows)
    points = 150
    array_cell = numpy.linspace(0, cell_length, points)

    array_temps = numpy.array([50, 60, 70, 80, 90, 100, 120])
    array_blue = numpy.array([2.301041, 2.430664, 2.463074, 2.592697, 2.916809, 3.532577, 4.634514])
    array_red = numpy.array([2.754761, 2.851982, 2.949218, 3.046455, 3.273301, 3.921494, 5.315086])

    array_fwhm = (array_blue + array_red) / 2

    array_colors = range(len(array_temps))
    my_colors = [(0, 0, 1), (0.3, 0.7, 0.3), (1, 0.8, 0), (1, 0, 0)]  # B -> Y -> R
    cmap_name = 'my_colors'

    cmap = colors.LinearSegmentedColormap.from_list(cmap_name, my_colors)
    plt.register_cmap(cmap=cmap)

    cmap = plt.get_cmap('my_colors')
    cnorm = colors.Normalize(vmin=0, vmax=len(array_temps)-1)
    scalar_map = mplcm.ScalarMappable(norm=cnorm, cmap=cmap)

    plot.set_latex()
    plot.set_fonts(10)

    fig = plot.canvas(size='twocol', aspect=1)
    ax1 = fig.add_subplot(111, aspect='equal', facecolor='#E0E0E0')
    plot.confax(ax1, side='left')

    ax1.set_xlabel("Position within the cell (mm)")
    ax1.set_ylabel("Intensity (arb. unit)")

    xpos = cell_length / 2 # cell center
    ypos = xpos
    aperture_diameter = 6

    ax1.add_patch(
        patches.Circle((xpos, ypos),
                       aperture_diameter / 2,
                       facecolor='#FFFFFF', edgecolor='#FFFFFF')
    )

    ax1.set_xlim(0, cell_length)
    ax1.set_ylim(0, cell_length)

    for idx, _ in enumerate(array_temps):
        color_val = scalar_map.to_rgba(array_colors[idx])
        intensity0 = 5.0 * array_fwhm[idx]
        array_intensity = intensity0 * \
            opticaldepth.calc_intensity_lambda(constants.K_D1,
                                               array_temps[idx]+constants.KELVIN,
                                               1e-3*array_cell)
        ax1.plot(array_cell, array_intensity,
                 label=r'{}\,C'.format(array_temps[idx]),
                 color=color_val)

    plt.legend(loc='lower center', bbox_to_anchor=[0.5, -0.48],
               ncol=3, shadow=False, title=None, fancybox=False)

    plot.save(fig, '../paper/bison_cell_transmission.pdf', tight=False)

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
