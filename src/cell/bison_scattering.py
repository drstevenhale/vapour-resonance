#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  BISON_CELL.PY
#
#    Steven Hale
#    2015 August 18
#    Birmingham, UK
#

"""Plots the scattering intensity for various detector apertures and
vapour temperatures.

.. image:: images/scatter_optimise.png
   :width: 600
   :alt: Plot of scattering intensity against vapour temperature and
         detector aperture.

"""

import numpy
import matplotlib.pyplot as plt

import core.plot as plot
import core.constants as constants
import core.opticaldepth as opticaldepth

########################################################################
########################################################################
########################################################################

def calc_active_region(temp, fwhm, cell_length, aperture_diameter):

    """
    Returns the reduction in intensity between two points in the cell,
    corresponding to the front and rear edge of the aperture diameter.
    """

    intensity0 = 1.0 * fwhm

    active_begin = (cell_length / 2) - (aperture_diameter / 2)
    active_end = (cell_length / 2) + (aperture_diameter / 2)

    begin_intensity = intensity0 * \
        opticaldepth.calc_intensity_lambda(constants.K_D1,
                                           temp+constants.KELVIN,
                                           1e-3*active_begin)
    end_intensity = intensity0 * \
        opticaldepth.calc_intensity_lambda(constants.K_D1,
                                           temp+constants.KELVIN,
                                           1e-3*active_end)

    return begin_intensity - end_intensity

########################################################################

def main():

    """
    Plots the scattering intensity for various detector apertures and
    vapour temperatures.
    """

    cell_length = 15 # m (17mm cell, 1mm windows)

    array_temps = numpy.array([50, 60, 70, 80, 90, 100, 120])
    array_blue = numpy.array([2.301041, 2.430664, 2.463074, 2.592697, 2.916809, 3.532577, 4.634514])
    array_red = numpy.array([2.754761, 2.851982, 2.949218, 3.046455, 3.273301, 3.921494, 5.315086])

    array_fwhm = (array_blue + array_red) / 2

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('narrow')
    ax1 = fig.add_subplot(111)
    plot.confax(ax1, side='left')

    #ax1.set_xlim(aTemps[0], aTemps[-1])
    ax1.set_ylim(0, 5.1)

    ax1.set_xlabel('Cell temperature (C)')
    ax1.set_ylabel('Intensity (arb. unit)')

    aperture_diameter = 6
    scatter_intensity = []
    for idx, _ in enumerate(array_temps):
        active = calc_active_region(array_temps[idx],
                                    array_fwhm[idx],
                                    cell_length,
                                    aperture_diameter)
        scatter_intensity.append(active)
    ax1.scatter(array_temps, scatter_intensity, s=10, color='m', label=r'6\,mm')
    ax1.plot(array_temps, scatter_intensity, linewidth=1, color='#FF99FF', zorder=0)

    aperture_diameter = 10
    scatter_intensity = []
    for idx, _ in enumerate(array_temps):
        active = calc_active_region(array_temps[idx],
                                    array_fwhm[idx],
                                    cell_length,
                                    aperture_diameter)
        scatter_intensity.append(active)
    ax1.scatter(array_temps, scatter_intensity, s=10, color='r', label=r'10\,mm')
    ax1.plot(array_temps, scatter_intensity, linewidth=1, color='#FF9999', zorder=0)

    aperture_diameter = 13
    scatter_intensity = []
    for idx, _ in enumerate(array_temps):
        active = calc_active_region(array_temps[idx],
                                    array_fwhm[idx],
                                    cell_length,
                                    aperture_diameter)
        scatter_intensity.append(active)
    ax1.scatter(array_temps, scatter_intensity, s=10, color='g', label=r'13\,mm')
    ax1.plot(array_temps, scatter_intensity, linewidth=1, color='#99FF99', zorder=0)

    aperture_diameter = 14
    scatter_intensity = []
    for idx, _ in enumerate(array_temps):
        active = calc_active_region(array_temps[idx],
                                    array_fwhm[idx],
                                    cell_length,
                                    aperture_diameter)
        scatter_intensity.append(active)
    ax1.scatter(array_temps, scatter_intensity, s=10, color='b', label=r'14\,mm')
    ax1.plot(array_temps, scatter_intensity, linewidth=1, color='#9999FF', zorder=0)

    aperture_diameter = 15
    scatter_intensity = []
    for idx, _ in enumerate(array_temps):
        active = calc_active_region(array_temps[idx],
                                    array_fwhm[idx],
                                    cell_length,
                                    aperture_diameter)
        scatter_intensity.append(active)
    ax1.scatter(array_temps, scatter_intensity, s=10, color='k', label=r'15\,mm')
    ax1.plot(array_temps, scatter_intensity, linewidth=1, color='#999999', zorder=0)

    plt.legend(loc='lower center', bbox_to_anchor=[0.5, -0.5],
               ncol=5, shadow=False, title=None, fancybox=False, fontsize=11)

    plot.save(fig, '../paper/scatter_optimise.pdf', tight=False)

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
