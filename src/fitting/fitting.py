#/bin/env python3
# -*- coding: utf-8 -*-
#
#  FITTING.PY
#
#    Steven Hale
#    2017 July 14
#    Birmingham, UK
#

"""Fits model against measured absorption profiles.

.. image:: images/fits.png
   :width: 600
   :alt: Plot of model fits to data.

"""

import numpy
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
from   scipy.optimize import curve_fit

import core.plot as plot
import core.constants as constants
import core.zeeman as zeeman
import core.opticaldepth as opticaldepth
from core.topticadlcdlpro import TopticaDLCDLpro

########################################################################
########################################################################
########################################################################

class SpectroscopyModel():

    """Fits model against measured absorption profiles."""

    def __init__(self, cell_temp):

        """Initialise the model."""

        self.fit = None
        self.fit_error = None
        self.data = {}
        self.cell_temp = cell_temp
        self.load_components()

########################################################################

    def load_components(self):

        """Load the requested absorption components."""

        scan = TopticaDLCDLpro(detector='transmit',
                               temp=self.cell_temp,
                               polarisation='blue')
        self.data = scan.get_data(calibrate_intensity=True, calibrate_piezo=True)

        scan = TopticaDLCDLpro(detector='transmit',
                               temp=self.cell_temp,
                               polarisation='red')
        data = scan.get_data(calibrate_intensity=True, calibrate_piezo=True)

        lambda0 = numpy.argmax(data['piezo'] > constants.K_D1)

        self.data['piezo'][lambda0:] = data['piezo'][lambda0:]
        self.data['intensity'][lambda0:] = data['intensity'][lambda0:]

########################################################################

    @staticmethod
    def generate_model(array_lambda, cell_temp, mag, broadening):

        """Generate the model."""

        cell_length = 15e-3 # m (17mm cell, 1mm windows)

        delta_nu = zeeman.b_to_deltanu(mag)
        delta_lambda = zeeman.deltanu_to_lambda(delta_nu, constants.K_D1)

        array_blue = opticaldepth.calc_intensity_lambda(array_lambda,
                                                        cell_temp + constants.KELVIN,
                                                        cell_length,
                                                        broadening=broadening,
                                                        lambda0=constants.K_D1-delta_lambda)

        array_red = opticaldepth.calc_intensity_lambda(array_lambda,
                                                       cell_temp + constants.KELVIN,
                                                       cell_length,
                                                       broadening=broadening,
                                                       lambda0=constants.K_D1+delta_lambda)

        lambda0 = numpy.argmax(array_lambda-constants.K_D1 > 0)

        array_red[0:lambda0] = array_blue[0:lambda0]

        return array_red

    def do_fit(self):

        """Do the fit."""

        cell_temp = 100
        mag = 0.3
        broadening = 1
        p0 = [cell_temp, mag, broadening]

        popt, pcov = curve_fit(self.generate_model, self.data['piezo'], self.data['intensity'], p0)
        perr = numpy.sqrt(numpy.diag(pcov))

        self.fit = popt
        self.fit_error = perr

    def plot_result(self, ax1, color_val):

        """Plot the results."""

        cell_temp = self.fit[0]
        temp_error = self.fit_error[0]

        mag = self.fit[1]
        b_error = self.fit_error[1]

        broadening = self.fit[2]
        broadening_error = self.fit_error[2]

        print(cell_temp, temp_error, cell_temp - self.cell_temp)
        print(mag, b_error)
        print(broadening, broadening_error)

        plot.confax(ax1)
        ax1.set_xlim([769.882, 769.914])
        ax1.set_ylim([0, 1.05])

        plot_every = 5
        ax1.scatter(self.data['piezo'][::plot_every]*1e9, self.data['intensity'][::plot_every],
                    color='k', marker='.', s=5, zorder=1)

        array_model = self.generate_model(self.data['piezo'], cell_temp, mag, broadening)

        ax1.plot(self.data['piezo']*1e9, array_model, color=color_val, linewidth=3, zorder=0)

        ax1.text(0.7, 0.50, r'Stem: {}\,C'.format(self.cell_temp),
                 transform=ax1.transAxes, color='k', horizontalalignment='right')
        ax1.text(0.7, 0.35, r'Cube: {}\,C'.format(self.cell_temp+20),
                 transform=ax1.transAxes, color='k', horizontalalignment='right')
        ax1.text(0.7, 0.20, r'Model: {:4.0f}\,C'.format(cell_temp),
                 transform=ax1.transAxes, color='k', horizontalalignment='right')

def main():

    """Plot all absorption temperatures."""

    plot.set_latex()
    plot.set_fonts()

    fig = plot.canvas('full')

    subplot = 421
    array_temps = [50, 60, 70, 80, 90, 100, 120, 130]

    array_colors = range(len(array_temps))

    my_colors = [(0, 0, 1), (0.3, 0.7, 0.3), (1, 0.8, 0), (1, 0, 0)]  # B -> Y -> R
    cmap_name = 'my_colors'

    #cm = colors.LinearSegmentedColormap.from_list(cmap_name, my_colors, N=len(array_temps))
    cmap = colors.LinearSegmentedColormap.from_list(cmap_name, my_colors)
    plt.register_cmap(cmap=cmap)

    cmap = plt.get_cmap('my_colors')
    c_norm = colors.Normalize(vmin=0, vmax=len(array_temps)-1)
    scalar_map = mplcm.ScalarMappable(norm=c_norm, cmap=cmap)

    for idx, _ in enumerate(array_temps):
        fit = SpectroscopyModel(array_temps[idx])
        fit.do_fit()
        ax = fig.add_subplot(subplot+idx)
        color_val = scalar_map.to_rgba(array_colors[idx])
        fit.plot_result(ax, color_val)

    plot.save(fig, '../paper/fits.pdf')

########################################################################
########################################################################
########################################################################

if __name__ == '__main__':
    main()

########################################################################
