% -*- coding: utf-8 -*-
%
% CALIBRATION.TEX
%
%   Steven Hale
%   2017 February 5
%   Birmingham, UK
%
% Zeeman splitting and magnetic field strength.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Laser Calibration}
\label{s:calibration}

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{coarse}
  \caption[Toptica DLC\,pro tunable diode laser profile]
  {Toptica DLC\,pro tunable diode laser profile tuning range, measured
  using an Agilent 86142B optical spectrum analyser.  The line width is
  much narrower than indicated in the figure.  The measured FWHM is
  broadened to the minimum~\SI{0.06}{\nano\metre} resolution bandwidth
  of the spectrum analyser.  The red and blue lines indicate the
  extent of the voltage-controlled piezo fine-tuning range after prior
  coarse tuning to approximately~\SI{769.9}{\nano\metre}.  The hatched
  area indicates the extent of our wavelength region of interest.}
\label{fig:laser_range}
\end{figure*}

We used a Toptica DLC\,pro tunable diode laser, which is a Littrow-style
external cavity diode laser (EDCL)~\citep{toptica}.  The installed
laser diode has an emission spectrum and tuning range
of~\SIrange{765}{805}{\nano\meter}. The primary tuning mechanism is by
altering the angle of incidence of the grating forming the external
cavity, which for coarse adjustment is done using a micrometer screw,
and for fine adjustment by varying the applied voltage to a piezo
actuator.  Tuning can also be achieved by changing the diode drive
current and temperature.  The diode suffers mode-hopping as the tuning
parameters are adjusted, but this can be mitigated by varying all
parameters simultaneously.  The typical mode-hop-free tuning-range
(MHFTR) is quoted at~\SIrange{20}{50}{\giga\hertz}, which at a
wavelength of~\SI{770}{\nano\metre} is equivalent
to~\SIrange{40}{98}{\pico\metre}.  Achieving this range can be tricky
due to the interaction of all the various tuning methods.  The typical
line width achieved by a Littrow-style ECDL over a duration
of~\SI{5}{\micro\second} is~\SIrange{10}{300}{\kilo\hertz}, which
again at a wavelength of~\SI{770}{\nano\metre} is less
than~\SI{0.6}{\femto\metre}.  The main contributions to the line width
are electronic noise, acoustic noise, and other vibrations that affect
the cavity length.

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{calibration1}
  \caption[Laser intensity flat-field calibration]
  {Laser intensity flat-field calibration.}
  \label{fig:calibration1}
\end{figure*}

The output of the ECDL has no intrinsic wavelength calibration, except
that the wavelength must be somewhere within the broadband output of
the model of installed laser diode.  In order to coarse-tune the laser
to approximately~\SI{769.9}{\nano\metre} the output wavelength was
measured using an Agilent 86142B optical spectrum
analyser~\citep{agilent_technical}, shown in
Figure~\ref{fig:laser_range}.  The 86142B has a resolution bandwidth
(RBW) of~\SI{0.06}{\nano\metre} and so the measured laser line width
is considerably broadened.  This resolution is sufficient to allow use
of the coarse manual micrometer screw adjustment to ensure the
required wavelength is positioned within the narrow scan range of the
piezo actuator.  The resolution is insufficient to tune the wavelength
precisely to the desired absorption line, but once coarse calibration
is achieved simply scanning between the piezo limits allows the
absorption line to be found.  In order to calibrate the width and
depth of the absorption line at varying temperature, it is necessary
to calibrate the piezo voltage in terms of wavelength and to normalise
the beam intensity.  This calibration is non-trivial and we will now
go on to discuss the steps required to achieve the final overall
calibration.

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{calibration2}
  \caption{Piezo voltage calibration after flat-field adjustment.  The
    line centres occur at~\SI{72.2}{\volt} and \SI{92.9}{\volt}.  The
    different depths are due to slight misalignment of the polarising
    filters.  There is also some evidence of non-linearity in the
    wavelength scan observed from slight differences in the profile
    widths.}
  \label{fig:calibration2}
\end{figure*}

Since BiSON spectrophotometers use a magnetic field to Zeeman split
the reference absorption line and produce two separate instrumental
passbands, this technique can be used to produce a known wavelength
separation and so calibrate the piezo.  Figure~\ref{fig:calibration1}
shows the two components of the Zeeman split absorption line with the
cell heated to~\SI{90}{\celsius} in a~\SI{0.3}{\tesla} longitudinal
magnetic field.  The beam intensity is not uniform across the piezo
scan due to a drive-current feed-forward mechanism employed to
increase the MHFTR of the diode laser.  It is imperative that the
diode does not mode-hop during the scan since this would result in
different wavelength calibrations either side of the mode-hop,
destroying the overall calibration.  The feed-forward adjusts the
drive-current proportionally with the piezo position-voltage by a
factor of~\SI{-0.32}{\milli\ampere\per\volt}, which helps prevent
mode-hopping but has the undesired side-effect of changing the beam
intensity.  If a mode-hop continues to appear at an inconvenient
wavelength, then the temperature of the diode can be adjusted to move
the mode-hop elsewhere.  The mean drive-current and detector-gain has
to be selected carefully to ensure that the current always stays above
the lasing threshold of~\SI{109}{\milli\ampere}, but also that the
beam intensity does not exceed the dynamic range of the detector
throughout the whole piezo scan range.  Once a mode-hop-free scan has
been achieved with a beam intensity that is comfortably within the
dynamic range of the detector, a flat-field can be captured and used
to normalise the intensity of the scan, shown in
Figure~\ref{fig:calibration2}.

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{transmit}
  \caption[Vapour cell absorption intensity profiles]
  {Vapour cell absorption intensity profiles at a range of
  temperatures.}
  \label{fig:transmission}
\end{figure*}

We can complete the calibration by making use of the known position of
the two Zeeman components, and applying the splitting to the piezo
voltages at the measured line centres.  The calibrated absorption
profiles for eight different cell temperatures are shown in
Figure~\ref{fig:transmission}.  There is some evidence of slight
misalignment of the polarising filters indicated by differences in
absorption depth between the two polarisation states.  There is also
some non-linearity in the wavelength scan indicated by a slight
difference in width between the two polarisation states.  These errors
are small and will be mitigated against by fitting the model to both
absorption components simultaneously, allowing all equal-and-opposite
errors to cancel.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
