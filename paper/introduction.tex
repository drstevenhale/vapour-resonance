% -*- coding: utf-8 -*-
%
% INTRODUCTION.TEX
%
%   Steven Hale
%   2017 August 6
%   Birmingham, UK
%
% Modelling vapour resonance.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

Resonance scattering techniques are often used to study the properties
of atoms and molecules, such as energy levels and the fine and
hyperfine structure~\citep{doi:10.1098/rsta.1979.0091}.  When applied
to absorption spectroscopy with techniques such as optical pumping
(so-called Saturated Absorption Spectroscopy) it is possible to
achieve a resolution that is Doppler-free and limited only by the
uncertainty principle, producing a highly stable locking reference for
tunable laser systems~\citep{doi:10.1119/1.18457,Svanberg2004}.
Resonance scattering spectroscopy has wide reaching multi-disciplinary
applications including biomedical diagnostics~\citep{Svanberg:16}, and
environmental sensing in terms of atmospheric
pollution~\citep{Svanberg:16}.  In astronomy, vapour reference cells
are often used to enable the high-precision spectrograph wavelength
calibration required for asteroseismology and detection of
exoplanets~\citep{Grundahl_2017, 10.1117/12.2056668}.

The Birmingham Solar Oscillations Network (BiSON) makes use of
resonance scattering with a potassium vapour reference cell, enabling
ultra-precise Doppler velocity measurements of oscillations of the
Sun~\citep{s11207-015-0810-0}.  In this article we present a model of
the resonance scattering properties of potassium vapour which can be
used to determine the ideal operating vapour temperature, and
spectrophotometer detector parameters, which are essential when
developing new instrumentation making use of off-the-shelf
components~\citep{halephd}.

In Section~\ref{s:instrumentation} we give a brief overview of BiSON
instrumentation and the use of resonance scattering spectroscopy.  In
Section~\ref{s:resonance} we review the theory of spectral line
formation from first principles, beginning from a similar position to
\citet{1993ExA.....4...87B} for sodium vapour, and in
Section~\ref{s:opticaldepth} develop the equations necessary to
determine optical depth from vapour temperature.  In
Sections~\ref{s:calibration} and~\ref{s:fitting} we use a tunable
diode laser to validate the model against the absorption profiles
observed with a standard BiSON vapour cell at a range of temperatures.
Finally in Section~\ref{s:detector} we use the model to determine the
ideal vapour temperature and detector configuration to maximise the
detected scattering intensity, and again validate against observed
scattering profiles.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
