% -*- coding: utf-8 -*-
%
% INSTRUMENTATION.TEX
%
%   Steven Hale
%   2017 February 5
%   Birmingham, UK
%
% BiSON Instrumentation
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{BiSON Instrumentation}
\label{s:instrumentation}

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{spectrophotometer}
  \caption{Schematic of a typical modern BiSON resonance scattering
    spectrophotometer.  Light travels from left to right.  The green
    boxes indicate detector locations.  The magnetic field across the
    vapour cell is longitudinal, with the field axis aligned with the
    optical axis.}
  \label{fig:bison_rss}
\end{figure*}

The original BiSON resonance scattering spectrophotometer (RSS)
developed for Doppler velocity astronomical observations is described
by~\citet{1978MNRAS.185....1B}.  Modern BiSON RSS follow the same
principles but make use of slightly different designs, and a typical
example is shown in Figure~\ref{fig:bison_rss}.  Light from the Sun is
first passed through a~\SI{30}{\milli\metre} diameter aperture, and a
pair of lenses in a Keplerian telescope arrangement are used to
compress the beam reducing the diameter in preparation for subsequent
smaller optics.  A red long-pass filter, and an infra-red short-pass
filter, form a pair to relieve the thermal load on the
instrumentation.  An interference filter with a bandwidth
of~\SI{1.5}{\nano\metre} is used to isolate only the potassium D1
Fraunhofer line at~\SI{769.898}{\nano\metre}, excluding the D2
line. The potassium is contained within a small cubic glass cell with
sides of length~\SI{17}{\milli\metre}, and a short stem.  The stem is
typically heated to around~\SI{90}{\degreeCelsius} to cause the
formation of potassium vapour within the cell, with the cubic cell
itself maintained at a temperature higher than this to ensure the
glass remains clear of solid potassium.  The cell is placed in a
longitudinal magnetic field which causes the absorption line to be
Zeeman split into two components, where their separation is dependent
on the magnetic field strength.  Splitting the lab reference-frame in
this way allows measurements to be taken at two working points on the
corresponding absorption line in the solar atmosphere.  The
polarisation component is selected using a linear polariser and an
electro-optic Pockels-effect quarter-wave retarder.  The polarisation
is switched at approximately~\SI{100}{\hertz} and a normalised ratio,
R, formed,
% 
\begin{equation}%\label{eq:ratio}
  R = \frac{I_{\mathrm{b}} - I_{\mathrm{r}}}{I_{\mathrm{b}} + I_{\mathrm{r}}} ~,
\end{equation}
%
where $I_{\mathrm{b}}$ and $I_{\mathrm{r}}$ are the measured
intensities in the blue and red wings respectively.  Normalising the
ratio in this way reduces the effect of atmospheric scintillation
allowing low-noise photometry through Earth's
atmosphere~\citep{hale2019measurement}, and also means it is
unnecessary to know the precise value of the magnetic field strength
or the precise frequency of the potassium line on the Sun.  Light is
focused into the centre of the potassium cell, where it is resonantly
scattered by the vapour.  Light that interacts with the vapour is
scattered isotropically, and it is this isotropic nature that is
leveraged by the {BiSON} resonance scattering spectrophotometers to
move a photon away from the instrumental optical axis and into a
detector. Two photodiode detectors on either side of the beam return a
precise measurement of the changing solar radial velocity.  A third
detector monitors unscattered light that passes directly through the
instrument.

There are several natural broadening processes which affect the
properties of an absorption line, and it is necessary to understand
the environmental factors (temperature and pressure) that affect these
processes when building a model of expected instrumentation
performance and informing new designs~\citep{halephd}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
