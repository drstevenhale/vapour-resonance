% -*- coding: utf-8 -*-
%
% OPTICALDEPTH.TEX
%
%   Steven Hale
%   2017 February 5
%   Birmingham, UK
%
% Optical depth calculations and cell tests.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Vapour Optical Depth}
\label{s:opticaldepth}

To be able to interpret the results of any experiments involving a gas
reference cell, we need a mathematical expression for the absorption
of the gas under the combined broadening conditions.  We know that no
matter what physical processes are responsible for the formation of a
spectral line, the integral of the absorption coefficient over all
frequencies must remain constant if the number of atoms remains
constant.  This can be explained more simply by considering that any
given atom can have the appropriate physical properties (i.e., speed,
direction) to interact with only one particular frequency.  If a
spectral line is broadened, then the absorption depth must
simultaneously reduce due to there being fewer atoms available to
interact at the line centre.  From this we can equate
equations~\ref{eq:intnatural2} and~\ref{eq:intdoppler} thus,
%
\begin{equation}%\label{eq:kv}
    \frac{\lambda_0^2}{8\pi}
    \frac{g_2}{g_1}
    \frac{N}{\tau}
    =
    \frac{1}{2}
    \sqrt{\frac{\pi}{\ln{2}}}
    \kappa_0 \Delta \nu ~,
\end{equation}
%
and solving for $\kappa_0$ produces~\cite[p.~100]{mitchell1961resonance},
%
\begin{equation}\label{eq:k0}
  \kappa_0 = \frac{2}{\Delta \nu}
             \sqrt{\frac{\ln{2}}{\pi}}
             \frac{\lambda^2_0}{8 \pi}
             \frac{g_2}{g_1}
             \frac{N}{\tau} ~,
\end{equation}
%
which is the maximum absorption possible.  This can be combined with
the expected line profile to produce the final value for $\kappa_\nu$.
We have already seen such an example in equation~\ref{eq:dopplerabs}
where for a line profile dominated by Doppler broadening $\kappa_\nu$
has a Gaussian profile.

The absorption coefficient and thickness in
equation~\ref{eq:absorptioncoeff} can be combined into one term
$\tau_\nu$, known as the optical depth,
%
\begin{equation}\label{eq:od}
  I_\nu = I_0 e^{-\tau_{\nu}} ~,
\end{equation}
%
where, as before, $I_0$ is the intensity of incident light, and
$I_\nu$ is the intensity of the transmitted light.  Unit optical depth
is therefore defined as the optical thickness that reduces the photon
flux by a factor of $1/e$.  It should be noted that $\tau_\nu$ is
specifically a different quantity from $\tau$ the lifetime of the
excited state.

We know that the \SI{769.898}{\nano\meter} line profile formed by
potassium vapour is dominated by Doppler broadening, and so
equation~\ref{eq:dopplerabs} is valid to describe the distribution.
In order to evaluate $\kappa_\nu$ we need to know the value of all the
terms in equation~\ref{eq:k0} -- temperature $T$, the atomic mass $m$,
the frequency and wavelength of the line centre $\nu_0$ and
$\lambda_0$, the number density of absorbing atoms $N$, the lifetime
of the excited state $\tau$, and the degeneracy of the
{$4^2P_{\frac{1}{2}}$\,--\,$4^2S_{\frac{1}{2}}$} energy levels $g_1$
and $g_2$, respectively.

\begin{figure*}[t]%
  \centering
  \includegraphics[scale=1.0]{vapour_pressure}
  \caption[Potassium vapour pressure model]
          {Upper: The blue crosses and green dots show measurements of
          the vapour pressure of potassium made independently
          by~\citet{520.full} and~\citet{1.1733889} at various
          temperatures.  The black line shows a cubic fit to the data.
          Lower: The residuals to the fit in terms of percentage
          (i.e., $100 \cdot (\textrm{model}/\textrm{data})$).  The
          uncertainty on the source data are not available, and so no
          error bars are plotted.}
  \label{fig:vapour_model}
\end{figure*}

We already know the lifetime $\tau$ to be~\SI{27}{\nano\second}, the
atomic mass to be~\SI{39.0983}{\atomicmassunit}, the central
wavelength to be~\SI{769.898}{\nano\meter}, and it may be
shown~\citep[p.~97]{mitchell1961resonance} that the ratio $g_2/g_1$ is
unity.  The temperature of the reference cell is controlled, and so
the only remaining term is the number density.  We can determine the
number density based on the vapour pressure, since as we saw earlier
it depends only on the temperature and not on the amount of physical
material.

The vapour in a gas reference cell can be treated as an ideal gas
since the pressure is by design very low.  The cells are evacuated and
baked out before being filled with a small amount of potassium.  Since
the pressure is low, there will be few interactions between particles.
The ideal gas law is defined as,
%
\begin{equation}\label{eq:idealgas1}
  pV = N k_\mathrm{B} T ~,
\end{equation}
%
where $p$ is the pressure, $V$ the volume, $N$ the number of atoms or
molecules, $k_\mathrm{B}$ the Boltzmann constant, and $T$ the
temperature.  If we combine the number of atoms and the volume into
one parameter to form the number density $n(T)$, and define the
pressure in terms of temperature then equation~\ref{eq:idealgas1}
simplifies to,
%
\begin{equation}%\label{eq:idealgas2}
  n(T) = \frac{p(T)}{k_\mathrm{B} T} ~,
\end{equation}
%
which depends only on the vapour pressure and the temperature.

Previous independent measurements have been
made~\citep{520.full,1.1733889} of the vapour pressure of potassium at
various temperatures.  These data have been used to fit a polynomial
in log space,
%
\begin{equation}\label{eq:vapour_model}
  p_\mathrm{vapour} = e^{a\,T^{3} + b\,T^{2} + c\,T + d} ~,
\end{equation}
%
where $p_\mathrm{vapour}$ is the vapour pressure and $T$ is the
temperature in Kelvin.  The values of the four fit coefficients are
shown in Table~\ref{table:vapour_model}.  A plot of results from this
model against the original data are shown in
Figure~\ref{fig:vapour_model}.  The model residuals are all
within~\SI{\pm5}{\percent} of the original value, with an upper limit
to the fit of~\SI{449}{\kelvin} (\SI{175}{\degreeCelsius}).  For a
typical vapour temperature of~\SI{100}{\degreeCelsius} the number
density required is~\SI{4.9e17}{\per\cubic\metre}.  This is equivalent
to~\SI{3.2e-8}{\kg} of potassium -- if you can see solid potassium in
the cell then there is enough to form the required vapour.

\begin{table}
    \centering
    \caption[Potassium vapour pressure model coefficients]
            {Potassium vapour pressure model coefficients.}
    \begin{tabular}{c S[table-format=-1.3e-1,round-mode=places,round-precision=3] S[table-format=-1.3e-1,round-mode=places,round-precision=3]}

    \toprule

    Coefficient & {Value} & {Uncertainty}\\

    \midrule

    a &  6.0054211235e-07 & 8.7468694167e-08\\
    b & -8.9121318942e-04 & 1.0072983768e-04\\
    c &  4.8897499745e-01 & 3.8399171195e-02\\
    d & -9.5551204080e+01 & 4.8436258495e+00\\

    \bottomrule

    \end{tabular}
    \label{table:vapour_model}
\end{table}

We now have everything in place to be able to calculate the optical
depth of a potassium vapour from equation~\ref{eq:od}.  We know all of
the parameters required to calculate the absorption coefficient
$\kappa_0$ in equation~\ref{eq:k0} by determining the vapour pressure
from equation~\ref{eq:vapour_model}, and we can calculate the
wavelength-dependent absorption coefficient from
equation~\ref{eq:dopplerabs}.  The model was verified by probing a
potassium vapour cell using a tunable diode laser.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
