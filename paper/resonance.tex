% -*- coding: utf-8 -*-
%
% RESONANCE.TEX
%
%   Steven Hale
%   2017 February 5
%   Birmingham, UK
%
% Theory of resonance radiation.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Resonance Radiation}
\label{s:resonance}

\subsection{Natural Line Width}

In a gas that forms an absorption line, the absorption coefficient
$\kappa_\nu$ of the gas is defined in terms of,
%
\begin{equation}\label{eq:absorptioncoeff}
  I_\nu = I_0 e^{-\kappa_\nu x} ~,
\end{equation}
%
where $I_0$ is the intensity of incident light, $I_\nu$ is the
intensity of the transmitted light, and $x$ is the thickness of the
absorbing material.  The units of the absorption coefficient are
always expressed in terms of the reciprocal of the units of $x$ such
that the exponent becomes a unitless ratio.

It can be shown~\citep[p.~95]{mitchell1961resonance} that the integral
of $\kappa_\nu$ over all frequencies is equal to,
%
\begin{equation}\label{eq:intnatural1}
  \int \kappa_\nu \mathrm{d} \nu =
    \frac{\lambda_0^2}{8\pi}
    \frac{g_2}{g_1}
    \frac{N}{\tau}
    \left( 1 - \frac{g_1}{g_2} \frac{N'}{N} \right) ~,
\end{equation}
%
where $\lambda_0$ is the wavelength at the centre of the absorption
line, $\tau$ the lifetime of the excited state, and $g_1$ and $g_2$
are the statistical weights of the normal and excited states
respectively.  $N$ is the number density of atoms of which
$[N-N']$ are capable of absorbing in the frequency range $\nu$ to
$\nu~+~\mathrm{d}\nu$, and similarly $N'$ is the number of excited
atoms that are capable of emitting in this range.  In the case where
the ratio of excited atoms to normal atoms is very small
($\leqslant~10^{-4}$) then $N'$ can be neglected and
equation~\ref{eq:intnatural1} reduces to,
%
\begin{equation}\label{eq:intnatural2}
  \int \kappa_\nu \mathrm{d} \nu =
    \frac{\lambda_0^2}{8\pi}
    \frac{g_2}{g_1}
    \frac{N}{\tau} ~,
\end{equation}
%
and this result shows that the integral of the absorption coefficient
is constant if $N$ remains constant.  This assumption can normally be
made when the formation of excited atoms is due to absorption of a
beam of light.  Where a gas is electrically excited at high current
densities, for example in a gas discharge lamp, the number of excited
atoms may become a high fraction of the total number and so the
simplification in equation~\ref{eq:intnatural2} cannot be made.  In
the case where all atoms capable of absorbing at a certain frequency
are already excited, the gas is said to be saturated and no further
absorption can take place at that frequency.

Although the energy of an atomic transition is fixed, and therefore
similarly the frequency of light with which it can interact, the
absorption profile has a natural line width which arises from the
uncertainty principle.  When considered in the form,
%
\begin{equation}%\label{eq:up1}
  \Delta E \Delta t > \frac{\hbar}{2} ~,
\end{equation}
%
where $\Delta E$ is the energy, $\Delta t$ the lifetime, and $\hbar$
the reduced Planck constant, the result suggests that for extremely
short excited state lifetimes there will be a significant uncertainty
in the energy of the photon emitted.  If the energy of many photons
are measured the distribution formed is Lorentzian in shape, where
$\Gamma$ is the width parameter for a Lorentzian profile. If we assume
the lifetime in the excited state is the uncertainty in time (i.e.,
$\tau = \Delta t$) then,
%
\begin{equation}\label{eq:up2}
  \Delta E = \frac{\Gamma}{2} = \frac{\hbar}{2 \tau} ~,
\end{equation}
%
and so,
%
\begin{equation}\label{eq:up3}
  \Gamma = \frac{\hbar}{\tau} ~,
\end{equation}
%
which shows that the natural line width is inversely
proportional to the lifetime of the atom in the excited
state.  \citet{PhysRev.77.153} observed the natural lifetime of the
potassium {$4^2P_{\frac{1}{2}}$\,--\,$4^2S_{\frac{1}{2}}$} transition
to be~\SI{27}{\nano\second}.  Using equation~\ref{eq:up3} this
corresponds to a Lorentzian width of~\SI{2.44e-8}{\electronvolt}.  We
can convert this to frequency using the relation $E = h\nu$, and so
equation~\ref{eq:up2} becomes,
%
\begin{equation}%\label{eq:up4}
  \Delta \nu   = \frac{1}{4 \pi \tau} ~,
\end{equation}
%
producing a width of~\SI{5.9}{\mega\hertz}.  For the line centre of
the {$4^2P_{\frac{1}{2}}$\,--\,$4^2S_{\frac{1}{2}}$} atomic transition
at~\SI{769.898}{\nano\meter} this is a full width at half maximum
({FWHM}) of just~\SI{1.166e-05}{\nano\meter}, which is equivalent to a
Doppler velocity of~\SI{4.54}{\meter\per\second}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Pressure Broadening}

Pressure acts to modify the natural line width.  When an excited atom
collides with another particle, it can be stimulated to decay earlier
than would be the case for its natural lifetime.  Continuing the
argument from the uncertainty principle as before, if the lifetime is
reduced than the uncertainty on the energy emitted must increase and
hence broaden the line width.  As the pressure increases, the
likelihood of collisions increases.

Experimental conditions are usually selected to ensure that pressure
broadening is minimised to a level of insignificance, and this is the
case for our vapour reference cells which are placed under vacuum
before being filled with their reference element.  We will neglect
this effect.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Doppler Broadening}

Since the atoms in a gas are moving, the wavelength of light absorbed
or emitted by the gas will be shifted according to the standard
non-relativistic Doppler formula,
%
\begin{equation}%\label{eq:doppler1}
  \frac{\Delta \lambda}{\lambda_0} = -\frac{\Delta \nu}{\nu_0} = \frac{v}{c} ~,
\end{equation}
%
where $\Delta\lambda$ is the shifted wavelength, $\lambda_0$ is the
rest wavelength, $v$ the speed, and $c$ the speed of light.
Similarly, $\Delta \nu$ is the shift in frequency, and $\nu_0$ is the
frequency at rest.  The atoms in a gas are moving at speeds defined by
the Maxwell-Boltzmann distribution, and so a spectral line will be
broadened into a range of possible wavelengths with a Gaussian
distribution. The FWHM due to Doppler broadening is given by,
%
\begin{equation}\label{eq:doppler6}
  \Delta \nu = \sqrt{\frac{8 k_\mathrm{B} T \ln{2}}{mc^2}} \nu_0
\end{equation}
%
which can be used to calculate the expected Doppler width of the
absorption line.  Potassium has a standard atomic weight
of~\SI{39.0983}{\atomicmassunit}.  For a reference vapour
at~\SI{100}{\celsius} the broadening is~\SI{0.0017}{\nano\meter}.
This is equivalent to a Doppler velocity
of~\SI{663}{\meter\per\second}, almost 150~times larger than the width
produced by natural broadening.  On the Sun at a temperature
of~\SI{5777}{\kelvin}, the D1 line width due to Doppler broadening
is~\SI{0.0067}{\nano\meter}.

It may be shown~\citep[p.~99]{mitchell1961resonance} that when
considering only Doppler broadening, the absorption coefficient of a
gas is given by,
%
\begin{equation}\label{eq:dopplerabs}
  \kappa_\nu = \kappa_0 e^{- \left[ \frac{2(\nu - \nu_0)}{\Delta \nu} \sqrt{\ln{2}} \right]^2} ~,
\end{equation}
%
where $\kappa_0$ is the ideal maximum absorption for Doppler
broadening alone, and $\Delta \nu$ is the Doppler breadth defined in
equation~\ref{eq:doppler6}.  The integral of $\kappa_\nu$ over all
frequencies is equal to,
%
\begin{equation}\label{eq:intdoppler}
  \int \kappa_\nu \mathrm{d} \nu
    = \frac{1}{2} \sqrt{\frac{\pi}{\ln{2}}} \kappa_0 \Delta \nu ~,
\end{equation}
%
and this depends only on the temperature and the atomic mass of the
atoms forming the vapour, which for a given material means temperature
is the only variable.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
