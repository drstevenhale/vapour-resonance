% -*- coding: utf-8 -*-
%
% DETECTOR.TEX
%
%   Steven Hale
%   2017 August 13
%   Birmingham, UK
%
% Detector parameters
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Instrumentation Parameters}

\label{s:detector}
\begin{figure}[t]
  \centering
  \includegraphics[scale=1.0]{bison_cell_transmission}
  \caption[Absorption through a~\SI{15}{\milli\metre} vapour cell at various temperatures]
  {Absorption through a typical~\SI{15}{\milli\metre} BiSON vapour
  cell at various temperatures.  The absorption coefficient is
  modelled as monochromatic but with the initial intensity scaled by
  the expected FWHM to compensate for the differences in spectral
  bandwidth.  The white circle in the centre indicates the
  typical~\SI{6}{\milli\metre} aperture of a scattering detector.}
  \label{fig:bison_cell}
\end{figure}

\begin{figure*}
  \centering
  \includegraphics[scale=1.0]{scatter_optimise}
  \caption[Optimum vapour temperature at various apertures]
  {Optimum vapour temperature for maximum scattering intensity at a
  range of aperture sizes.}
  \label{fig:scatter_optimise}
\end{figure*}

The model can be used to estimate the scattering performance of the
cell and the effect of different detector aperture sizes and
positions.  Figure~\ref{fig:bison_cell} shows the expected intensity
change as light passes through the cell determined from
equation~\ref{eq:absorptioncoeff}.  The absorption coefficient is
modelled as monochromatic from equation~\ref{eq:k0} but with the
initial intensity scaled by the expected FWHM to compensate for the
differences in spectral bandwidth.  We make an assumption that any
photon is scattered only once.  Any secondary (or further multiple)
scattering of light that has been moved away from the optical axis in
the direction of a scattering detector will be isotropic. The detected
intensity will be reduced by the optical depth between the point of
scattering within the cell and the detector, and so it is important
when calculating the total expected throughput, but has only a minor
effect when considering variations in aperture size.  The white circle
in the centre of the figure indicates the typical~\SI{6}{\milli\metre}
aperture of a scattering detector, with scattered light perpendicular
to the page coming out towards the reader as the beam passes through
from left to right. It is clear that at low temperatures there is very
little scattered light due to the small absorption coefficient.  At
high temperatures almost all the light is scattered at the front of
the cell and so not captured by the detector.  This implies that there
will be some optimum temperature at which the measured scattering
intensity is greatest.  By considering the change in intensity within
the area of the white circle, we can estimate the intensity of light
scattered away from the beam optical axis.
Figure~\ref{fig:scatter_optimise} shows the estimated scattering
intensity for several aperture sizes and vapour temperatures.  If the
aperture is equal to the size of the vapour cell then all the light
scattered towards the detector is captured, and the intensity
continually increases with temperature.  As the aperture size is
reduced a temperature plateau forms.  One might expect that the ideal
aperture is as large as possible in order to maximise the intensity of
scattered light and so maximise the signal-to-noise ratio.  However,
there are several reasons why this is not the case.  Applying a
smaller aperture and causing the temperature plateau to form minimises
the effect of fluctuations in vapour temperature, and the
temperature-width of the plateau is greatest at smaller apertures.
Also, it is imperative to eliminate non-resonantly scattered light
from reaching the detector, such as specular reflections from the cell
glass especially at the edges and corners of the cell, and again this
is best achieved via small apertures effectively acting as a baffle.

\begin{figure*}
  \centering
  \includegraphics[scale=1.0]{scatter}
  \caption[Vapour cell scattering intensity profiles]
  {Vapour cell scattering intensity profiles at a range of
  temperatures. The profile asymmetry seen at temperatures above the
  ideal temperature is a result of the inhomogeneity of the magnetic
  field.}
  \label{fig:scattering}
\end{figure*}

The scattering detectors used in most BiSON instrumentation
employ~\SIrange{6}{10}{\milli\metre} circular apertures which suggests
that the maximum intensity will be found at a stem temperature of
approximately~\SIrange{80}{90}{\celsius}.  Many {BiSON} instruments
run at a stem temperature of~\SI{90}{\celsius} and a cube temperature
of~\SI{110}{\celsius} and so these results agree with expectation
based on experience.  At this temperature the optical depth is
approximately one if measured from the front of the cell to the
centre, or approximately two from the front of the cell to the rear.

Figure~\ref{fig:scattering} shows the scattering profiles taken
simultaneously with the absorption profiles shown in
Figure~\ref{fig:transmission} over a range of vapour cell
temperatures, and the intensity peaks at
approximately~\SIrange{80}{90}{\celsius} as expected.  The profile
asymmetry seen at high temperatures is a result of the inhomogeneity
of the magnetic field. At lower temperatures, light is detected from
across the whole aperture, and the peaks are centred at the
wavelengths corresponding to the splitting produced by the mean field
strength.  At higher temperatures, the scattering is dominated towards
the front of the cell within the weaker part of the field, and not
seen by the detector.  The detector captures only the remaining small
fraction of light at the wavelength corresponding to the higher field
strength at the centre of the cell, leading to the observed offset
weighted towards higher field strength.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
