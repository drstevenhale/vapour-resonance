% -*- coding: utf-8 -*-
%
% FITTING.TEX
%
%   Steven Hale
%   2017 August 13
%   Birmingham, UK
%
% Fitting potassium model with the tunable laser.
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Model Validation}
\label{s:fitting}

\begin{figure*}[t]
  \centering
  \includegraphics[scale=1.0]{magnet}
  \caption{On-axis magnetic variation inside the cell oven.  The field
    inhomogeneity adds an additional source of spectral broadening
    which must be included in the model.}
  \label{fig:magnet}
\end{figure*}

\begin{figure*}[p]
  \centering
  \includegraphics[scale=1.0]{fits}
  \caption[Modelled absorption profiles at a range of vapour
    temperatures] {Modelled absorption profiles at a range of vapour
    temperatures.  The dotted lines show the measured absorption
    profile.  The solid lines show the results of the fit to the data.
    Both absorption components are fitted simultaneously allowing
    systematic equal-and-opposite errors to cancel.}
  \label{fig:model}
\end{figure*}

\begin{table*}
    \centering
    \sisetup{table-format=3.1,round-minimum=0.1,round-mode=places,round-precision=1}
    \caption{Fitted model parameters at a range of vapour temperatures.}
    \begin{tabular}{S[table-format=3.0]
                    S[table-format=3.0]
                    S[table-format=3.0,round-precision=0]
                    S[table-format=<1.1]
                    S[table-format=-2.1]
                    S[table-format=1.2,round-precision=2]
                    S[table-format=<1.2,round-precision=2,round-minimum=0.01]}

    \toprule
        \multicolumn{2}{c}{{\bfseries Oven Temp}} & \multicolumn{3}{c}{{\bfseries Model Temp}} & \multicolumn{2}{c}{{\bfseries Broadening Factor}}\\
        \cmidrule(lr){1-2}\cmidrule(lr){3-5}\cmidrule(lr){6-7}
        {Stem} & {Cube} & {Stem} & {Uncertainty} & {$\Delta T$} & {Scale} & {Uncertainty}\\
    \midrule

     50 &  70 &  49.93801683529483 & 0.05911385274160247  &  -0.06198316470516829 & 1.5497977516900034 & 0.010225235529756882\\
     60 &  80 &  59.07193440488454 & 0.05806678516341524  &  -0.9280655951154628  & 1.593463750618715  & 0.009660575562317709\\
     70 &  90 &  67.8763995136866  & 0.05387503498457559  &  -2.123600486313407   & 1.551258089828436  & 0.008040919986667729\\
     80 & 100 &  76.78513535292292 & 0.049740470851325234 &  -3.2148646470770785  & 1.4761742475672481 & 0.006132750128753006\\
     90 & 110 &  87.3266062018605  & 0.06018561298799116  &  -2.6733937981395     & 1.3954584936066434 & 0.004645848115386062\\
    100 & 120 &  97.80252696851991 & 0.14375630603606684  &  -2.1974730314800865  & 1.3611303163197057 & 0.005682520990971153\\
    120 & 140 & 114.12485974429399 & 0.605734316440814    &  -5.875140255706015   & 1.4125803194248847 & 0.01311016578462419\\
    130 & 150 & 119.23633188810875 & 0.8460268659074462   & -10.76366811189125    & 1.4694733287114523 & 0.01670930894195373\\
    
    \bottomrule

    \end{tabular}
    \label{table:fitted_temperatures}
\end{table*}

The laser calibration is dependent on the accuracy and uniformity of
the vapour cell longitudinal magnetic field.  The field strength at
several points inside the cell oven was measured using a Hirst GM04
Gaussmeter, which is factory calibrated using nuclear magnetic
resonance to correct for irregularities in the supplied semi-flexible
transverse Hall probe.  It was found that the target field strength
of~\SI{0.3}{\tesla} is achieved only at the cell centre and drops to
approximately~\SI{0.26}{\tesla} at the front and rear of the cell
oven, as shown in Figure~\ref{fig:magnet}.  The mean field strength
is~\SI{0.283}{\tesla}.  Due to the variation in field strength, it is
not necessary to measure the field strength to high precision, however
this does mean that the absorption profile will have an additional
broadening factor caused by the non-uniform magnetic field, and the
FWHM determined from equation~\ref{eq:doppler6} has to be modified to
allow an additional degree of freedom.

The resulting final model has been fitted against absorption profiles
for all eight vapour temperatures in Figure~\ref{fig:transmission}.
Both absorption components are fitted simultaneously allowing
systematic equal-and-opposite errors to cancel.  The results are shown
in Figure~\ref{fig:model}, where the dotted lines show the measured
absorption profile and the solid lines show the results of the fit to
the data.  The fitted parameters and uncertainties listed in
Table~\ref{table:fitted_temperatures}.

All fitted temperatures indicate that the vapour pressure is
controlled by the temperature of the solid potassium in the stem of
the cell.  The temperature of the upper cube of the cell simply has to
be higher than the stem and high enough to ensure that deposition of
solid potassium onto the windows does not occur.  The increasing
difference between the model temperature and the stem temperature at
higher values is an indication of the difficulties of coupling heat
efficiently into the cell.  Heating of the stem is achieved by a foil
heating element wrapped around the glass, and heat does of course leak
into the surrounding chassis as well as heating the cell.  At higher
temperatures the losses become more significant and it becomes
progressively more difficult to effectively heat the cell, and this is
indicated by the real temperature of the vapour as determined from the
model being a few degrees below the measured temperature of the stem
heating element.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
