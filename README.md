# Modelling the response of potassium vapour in resonance scattering spectroscopy

[![pipeline status](https://gitlab.com/drstevenhale/vapour-resonance/badges/dev/pipeline.svg)](https://gitlab.com/drstevenhale/vapour-resonance/commits/dev)
[![coverage report](https://gitlab.com/drstevenhale/vapour-resonance/badges/dev/coverage.svg)](https://gitlab.com/drstevenhale/vapour-resonance/commits/dev)
[![arXiv](https://img.shields.io/badge/astro--ph.IM-arXiv%3A2002.04546-B31B1B)](https://arxiv.org/abs/2002.04546)
[![eData](https://img.shields.io/badge/UBIRA%20eData-doi%3Aedata.bham.00000417-605270)](https://doi.org/10.25500/edata.bham.00000417)
[![IOP](https://img.shields.io/badge/IOP-doi%3A10.1088%2F1361--6455%2Fab7529-006eb2)](https://doi.org/10.1088%2F1361-6455%2Fab7529)

A repository containing the paper, data, and code for the publication
"[Modelling the response of potassium vapour in resonance scattering
spectroscopy](https://doi.org/10.1088%2F1361-6455%2Fab7529)" published
by [Journal of Physics B: Atomic, Molecular and Optical
Physics](https://iopscience.iop.org/journal/0953-4075).

The accepted version of the paper is availble from
[arXiv](https://arxiv.org/abs/2002.04546).  The data are also
available via the University of Birmingham [eData
archive](https://doi.org/10.25500/edata.bham.00000417).

# Citation

If you wish to make use of these data, please cite both the paper and
the data repository using the following BibTeX:

```
@ARTICLE{2020JPhB...53h5003H,
       author = {{Hale}, S.~J. and {Chaplin}, W.~J. and {Davies}, G.~R. and
         {Elsworth}, Y.~P.},
        title = "{Modelling the response of potassium vapour in resonance scattering spectroscopy}",
      journal = {Journal of Physics B Atomic Molecular Physics},
     keywords = {resonance scattering spectroscopy, optical depth, vapour reference cell, model},
         year = 2020,
        month = apr,
       volume = {53},
       number = {8},
          eid = {085003},
        pages = {085003},
          doi = {10.1088/1361-6455/ab7529},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2020JPhB...53h5003H},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
```

```
@misc{edata417,
      title = {{Modelling the response of potassium vapour in resonance scattering spectroscopy}},
     author = {{Hale}, S.~J},
  publisher = {Birmingham Solar Oscillations Network},
       year = {2020},
      month = jan,
   keywords = {resonance scattering spectroscopy, optical depth, vapour reference cell, model},
        doi = {10.25500/edata.bham.00000417},
        url = {https://doi.org/10.25500/edata.bham.00000417}
}
```
